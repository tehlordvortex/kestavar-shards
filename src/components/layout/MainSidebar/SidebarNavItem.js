import React from 'react'
import PropTypes from 'prop-types'
import { NavLink as RouteNavLink } from 'react-router-dom'
import { NavItem, NavLink, Collapse } from 'shards-react'
import OpenCloseIcon from '../../common/OpenCloseIcon'
import { useUser } from '../../../contexts/user'
import { isAuthorized, AuthorizationType } from '../../../utils/roles'

const SidebarNavDropdown = ({ item, match }) => {
  const [open, setOpen] = React.useState(false)
  const user = useUser()
  const { permissions = [] } = item
  const toggle = () => {
    setOpen(open => !open)
  }
  if (
    !isAuthorized(
      user.permissionsArray,
      permissions,
      AuthorizationType.oneOrMore
    )
  )
    return null
  return (
    <>
      <NavItem className="cursor-pointer" onClick={toggle}>
        <NavLink tag="div">
          {item.htmlBefore && (
            <div
              className="d-inline-block item-icon-wrapper"
              dangerouslySetInnerHTML={{ __html: item.htmlBefore }}
            />
          )}
          {item.title && <span>{item.title}</span>}
          <OpenCloseIcon className="float-right" open={open} />
        </NavLink>
      </NavItem>
      <Collapse open={open}>
        {item.children.map(child => (
          <SidebarNavItem item={child} key={child.to} hasParent />
        ))}
      </Collapse>
    </>
  )
}

const SidebarNavItem = ({ item, hasParent }) => {
  const user = useUser()
  const { permissions = [] } = item
  // Defer authorization check for dropdowns to the component itself
  if (!item.children && !isAuthorized(user.permissionsArray, permissions))
    return null
  return !!item.children ? (
    <SidebarNavDropdown item={item} />
  ) : (
    <NavItem>
      <NavLink tag={RouteNavLink} exact={true} to={item.to}>
        {item.htmlBefore && (
          <div
            className="d-inline-block item-icon-wrapper"
            dangerouslySetInnerHTML={{ __html: item.htmlBefore }}
          />
        )}
        {item.title && (
          <span className={hasParent ? 'd-inline-block ml-4' : undefined}>
            {item.title}
          </span>
        )}
        {item.htmlAfter && (
          <div
            className="d-inline-block item-icon-wrapper"
            dangerouslySetInnerHTML={{ __html: item.htmlAfter }}
          />
        )}
      </NavLink>
    </NavItem>
  )
}

SidebarNavItem.propTypes = {
  /**
   * The item object.
   */
  item: PropTypes.object,
}

export default SidebarNavItem
