import React from 'react'
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink,
} from 'shards-react'
import { getUserData, logoutUser } from '../../../../utils/auth'
import { withRouter } from 'react-router-dom'

class UserActions extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      visible: false,
    }

    this.toggleUserActions = this.toggleUserActions.bind(this)
  }

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible,
    })
  }

  _logout = () => {
    const { history } = this.props
    logoutUser(history)
  }

  render() {
    const userDetails = getUserData()
    return (
      <NavItem
        tag={Dropdown}
        caret
        toggle={this.toggleUserActions}
        className="cursor-pointer"
      >
        <DropdownToggle
          caret
          tag={NavLink}
          className="text-nowrap px-3 d-flex align-items-center h-100"
        >
          <div className="user-avatar d-flex align-items-center mr-2">
            <i className="material-icons">account_circle</i>
          </div>
          <span className="d-inline-block">{userDetails.username}</span>
        </DropdownToggle>
        <Collapse tag={DropdownMenu} right small open={this.state.visible}>
          <DropdownItem href="#" onClick={this._logout} className="text-danger">
            <i className="material-icons text-danger">&#xE879;</i> Logout
          </DropdownItem>
        </Collapse>
      </NavItem>
    )
  }
}

export default withRouter(UserActions)
