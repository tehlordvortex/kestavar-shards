import React from 'react'
import classnames from 'classnames'
import DatePicker from 'react-datepicker'

import 'react-datepicker/dist/react-datepicker.css'
export const DatePickerField = ({ name, value, onChange, invalid, placeholder = "Select date...", ...props }) => (
  <DatePicker
    className={classnames('form-control', invalid && 'is-invalid')}
    selected={(value && new Date(value)) || null}
    onChange={date => onChange(name, date)}
    placeholderText={placeholder}
    {...props}
  />
)
