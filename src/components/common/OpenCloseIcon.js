import React from 'react'
import classnames from 'classnames'

const OpenCloseIcon = ({ open, className, ...props }) => (
  <i className={classnames(className, 'material-icons')} {...props}>
    {open ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}
  </i>
)

export default OpenCloseIcon
