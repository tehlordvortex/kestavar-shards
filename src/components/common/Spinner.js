import React from 'react'
import classnames from 'classnames'

const Spinner = ({ className }) => (
  <>
    <span
      className={classnames(className, 'spinner-border spinner-border-sm')}
      role="status"
      aria-hidden="true"
    />
    <span className="sr-only">Loading...</span>
  </>
)

export default Spinner
