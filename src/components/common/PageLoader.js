import React from 'react'

const PageLoader = () => (
  <div className="page-loader"></div>
)

export default PageLoader