import React from 'react'
import Errors from '../../views/Errors';

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, info) {
    // You can also log the error to an error reporting service
    if (process.env.NODE_ENV === "development") {
      console.log(error, info)
    }
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <Errors />
    }

    return this.props.children;
  }
}