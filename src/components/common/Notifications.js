import React, { createContext, useContext, useState, useCallback, useMemo, useRef } from 'react'
import { NotificationStack } from 'react-notification'

const NotificationsContext = createContext({})

function styleFactory(index, style, notification) {
  return Object.assign(
    {},
    style,
    {
    	bottom: `${2 + (index * 7)}rem`,
    	right: `1rem`,
    	left: `unset`
    }
  );
}

export const NotificationsProvider = ({ children }) => {
	const [notifications, setNotifications] = useState([])
	const count = useRef(0)
	const addNotification = notification => {
		const key = count.current
		setNotifications(notifications => ([
			...notifications,
			{
				key,
				...notification
			}
		]))
		count.current += 1
		return key
	}
	const removeNotification = id => {
		setNotifications(notifications => {
			const newNotifs = notifications.filter(notification => notification.key !== id)
			count.current = newNotifs.length
			return newNotifs
		})
	}
	const contextValue = {
		notifications,
		addNotification,
		removeNotification
	}
	return (
		<NotificationsContext.Provider value={contextValue}>
			{children}
		</NotificationsContext.Provider>
	)
}

export const useNotifications = () => useContext(NotificationsContext)

export const NotificationTray = () => {
	const { notifications, removeNotification } = useNotifications()
	const onDismiss = (notification) => removeNotification(notification.key)
	return (
		<NotificationStack
			notifications={notifications}
			onDismiss={onDismiss}
			barStyleFactory={styleFactory}
		/>
	)
}