import { useUser } from '../../contexts/user'
import { isAuthorized } from '../../utils/roles'

const PermissionGuard = ({ permissions = [], children }) => {
  const user = useUser()
  const show =
    permissions.length > 0 && isAuthorized(user.permissionsArray, permissions)
  return children({ show })
}

export default PermissionGuard
