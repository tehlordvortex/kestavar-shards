import React from "react";
import { Container } from "shards-react";

const EmptyLayout = ({ children }) => (
  <Container
    fluid
    className="empty-layout-container d-flex flex-column align-items-center justify-content-center"
  >
    {children}
  </Container>
);

export default EmptyLayout;
