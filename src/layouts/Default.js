import React, { Suspense } from 'react'
import PropTypes from 'prop-types'
import { Container, Row, Col } from 'shards-react'
import { Redirect } from 'react-router-dom'

import MainNavbar from '../components/layout/MainNavbar/MainNavbar'
import MainSidebar from '../components/layout/MainSidebar/MainSidebar'
import MainFooter from '../components/layout/MainFooter'
import { getAuthToken } from '../utils/auth'
import PageLoader from '../components/common/PageLoader';

const DefaultLayout = ({ children, noNavbar, noFooter }) => {
  const token = getAuthToken()
  if (!token) {
    return <Redirect to="/auth/login" />
  }
  return (
    <Container fluid>
      <Row>
        <MainSidebar />
        <Col
          className="main-content p-0"
          lg={{ size: 10, offset: 2 }}
          md={{ size: 9, offset: 3 }}
          sm="12"
          tag="main"
        >
          {!noNavbar && <MainNavbar />}
          <Suspense fallback={
            <Container fluid className="main-content-container px-5">
              <PageLoader />
            </Container>
          }>
            {children}
          </Suspense>
          {!noFooter && <MainFooter />}
        </Col>
      </Row>
    </Container>
  )
}

DefaultLayout.propTypes = {
  /**
   * Whether to display the navbar, or not.
   */
  noNavbar: PropTypes.bool,
  /**
   * Whether to display the footer, or not.
   */
  noFooter: PropTypes.bool,
}

DefaultLayout.defaultProps = {
  noNavbar: false,
  noFooter: false,
}

export default DefaultLayout
