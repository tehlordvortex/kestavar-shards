import React, {
  createContext,
  useContext,
  useEffect,
  useState,
  useMemo,
} from 'react'
import { getUserData } from '../utils/auth'

export const UserContext = createContext({})

export const UserProvider = ({ children }) => {
  const [userData, setUserData] = useState(
    getUserData() || {
      username: '',
      role: {
        roleId: '',
        roleName: '',
        permissions: '',
      },
    }
  )
  const permissionsArray = useMemo(() => {
    if (userData && userData.role)
      return userData.role.permissions.split(',')
    else return []
  }, [userData && userData.role && userData.role.permissions])
  const updateUserData = (...args) => {
    setUserData(getUserData())
  }
  useEffect(() => {
    document.addEventListener('localStorageChange', updateUserData)
    return () => {
      document.removeEventListener('localStorageChange', updateUserData)
    }
  })

  return (
    <UserContext.Provider
      value={{
        ...userData,
        permissionsArray,
      }}
    >
      {children}
    </UserContext.Provider>
  )
}

export const useUser = () => useContext(UserContext)
export const withUser = (Component) => {
  const user = useUser()
  return (props) => (
    <Component user={user} {...props} />
  )
}
