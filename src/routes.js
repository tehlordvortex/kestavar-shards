import React, { lazy } from 'react'
import { Redirect } from 'react-router-dom'

// Layout Types
import { DefaultLayout, EmptyLayout } from './layouts'

// Route Views

import Error404 from './views/Erro404'
import Error401 from './views/Error401'

const Login = lazy(() => import('./views/Login'))
const Dashboard = lazy(() => import('./views/Dashboard'))
const BuyPower = lazy(() => import('./views/BuyPower'))
const Transactions = lazy(() => import('./views/Tansactions'))
const Transaction = lazy(() => import('./views/Transaction'))
const Borrow = lazy(() => import('./views/Borrow'))
const Customers = lazy(() => import('./views/Customers'))
const Roles = lazy(() => import('./views/Roles'))
const RoleEditor = lazy(() => import('./views/RoleEditor'))
const Users = lazy(() => import('./views/Users'))
const UserEditor = lazy(() => import('./views/UserEditor'))
const Share = lazy(() => import('./views/Share'))
const Wallet = lazy(() => import('./views/Wallet'))

export default [
  {
    path: '/app',
    layout: DefaultLayout,
    children: [
      {
        path: '/',
        exact: true,
        component: () => <Redirect to="/app/dashboard" />,
      },
      {
        path: '/dashboard',
        component: Dashboard,
      },
      {
        path: '/power/buy',
        component: BuyPower,
      },
      {
        path: '/power/borrow',
        component: Borrow,
      },
      {
        path: '/transactions',
        component: Transactions,
        exact: true,
        permissions: ['GET_TRANSACTIONS'],
      },
      {
        path: '/transactions/:id',
        component: Transaction,
        permissions: ['GET_TRANSACTION'],
      },
      {
        path: '/share',
        component: Share,
        permissions: ['GET_TRANSACTIONS'],
      },
      {
        path: '/customers',
        component: Customers,
        exact: true,
        permissions: ['GET_CUSTOMERS'],
      },
      {
        path: '/roles',
        component: Roles,
        exact: true,
        permissions: ['GET_ROLES'],
      },
      {
        path: '/roles/create',
        component: RoleEditor,
        exact: true,
        permissions: ['CREATE_ROLE'],
      },
      {
        path: '/roles/:id/edit',
        component: props => (
          <RoleEditor id={props.match.params.id} {...props} />
        ),
        permissions: ['CREATE_ROLE'],
      },
      {
        path: '/users',
        component: Users,
        exact: true,
        permissions: ['GET_USERS'],
      },
      {
        path: '/users/create',
        component: UserEditor,
        exact: true,
        permissions: ['CREATE_USER'],
      },
      {
        path: '/users/:id/edit',
        component: props => (
          <UserEditor id={props.match.params.id} {...props} />
        ),
        permissions: ['CREATE_USER'],
      },
      {
        path: '/wallet',
        component: Wallet,
        exact: true,
        permissions: ['ALL']
      },
      {
        path: '/error/unauthorized',
        component: Error401,
        exact: true,
      },
      {
        component: Error404,
      },
    ],
  },
  {
    path: '/auth',
    layout: EmptyLayout,
    children: [
      {
        path: '/login',
        component: Login,
      },
      {
        component: Error404,
      },
    ],
  },
  {
    path: '/',
    layout: EmptyLayout,
    children: [
      {
        path: '/',
        exact: true,
        component: () => <Redirect to="/app" />,
      },
      {
        component: Error404,
      },
    ],
  },
]
