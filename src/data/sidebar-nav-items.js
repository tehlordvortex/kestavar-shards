export default function() {
  return [
    {
      title: 'Dashboard',
      to: '/app/dashboard',
      htmlBefore: '<i class="material-icons">dashboard</i>',
      htmlAfter: '',
    },
    {
      title: 'Power',
      htmlBefore: '<i class="material-icons">power</i>',
      children: [
        {
          title: 'Buy Power',
          to: '/app/power/buy',
        },
        {
          title: 'Borrow Power',
          to: '/app/power/borrow',
        },
      ],
    },
    {
      title: 'Transactions',
      htmlBefore: '<i class="material-icons">view_list</i>',
      permissions: ['GET_TRANSACTIONS'],
      children: [
        {
          to: '/app/transactions',
          title: 'All transactions',
          permissions: ['GET_TRANSACTIONS']
        },
        {
          to: '/app/share',
          title: 'Reseller share',
          permissions: ['GET_TRANSACTIONS']
        }
      ]
    },
    {
      title: 'Customers',
      htmlBefore: '<i class="material-icons">supervisor_account</i>',
      permissions: ['GET_CUSTOMERS'],
      children: [
        {
          to: '/app/customers',
          title: 'All customers',
          permissions: ['GET_CUSTOMERS']
        },
      ]
    },
    {
      title: 'Roles',
      htmlBefore: '<i class="material-icons">supervisor_account</i>',
      permissions: ['GET_ROLES', 'CREATE_ROLE'],
      children: [
        {
          to: '/app/roles',
          title: 'All roles',
          permissions: ['GET_ROLES']
        },
        {
          to: '/app/roles/create',
          title: 'Create role',
          permissions: ['CREATE_ROLE']
        }
      ]
    },
    {
      title: 'Users',
      htmlBefore: '<i class="material-icons">supervisor_account</i>',
      permissions: ['GET_USERS', 'CREATE_USER'],
      children: [
        {
          to: '/app/users',
          title: 'All users',
          permissions: ['GET_USERS']
        },
        {
          to: '/app/users/create',
          title: 'Create user',
          permissions: ['CREATE_USER']
        }
      ]
    },
    {
      title: 'Wallet',
      htmlBefore: '<i class="material-icons">account_balance_wallet</i>',
      permissions: ['ALL'],
      to: '/app/wallet',
    },
    // {
    //   title: "Blog Posts",
    //   htmlBefore: '<i class="material-icons">vertical_split</i>',
    //   to: "/blog-posts",
    // },
    // {
    //   title: "Add New Post",
    //   htmlBefore: '<i class="material-icons">note_add</i>',
    //   to: "/add-new-post",
    // },
    // {
    //   title: "Forms & Components",
    //   htmlBefore: '<i class="material-icons">view_module</i>',
    //   to: "/components-overview",
    // },
    // {
    //   title: "Tables",
    //   htmlBefore: '<i class="material-icons">table_chart</i>',
    //   to: "/tables",
    // },
    // {
    //   title: "User Profile",
    //   htmlBefore: '<i class="material-icons">person</i>',
    //   to: "/user-profile-lite",
    // },
    // {
    //   title: "Errors",
    //   htmlBefore: '<i class="material-icons">error</i>',
    //   to: "/errors",
    // }
  ]
}
