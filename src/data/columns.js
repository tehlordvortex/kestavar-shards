import moment from "moment";
import { getAccountTypeName } from "../utils/getAccountTypeName";
import { formatCurrency } from '../utils/formatCurrency';
import { getCommissionForAmount, getKestavarCommissionForAmount } from '../utils/getCommissionForAmount';

// Workaround to avoid Excel reading the value as a number
const workaroundExcel = value => `${value}\t`

const firstBankCommColumn = {
  name: "First Bank Comm",
  selector: "amount",
  format: row => formatCurrency(getCommissionForAmount(row.token, 1.1)),
  sortable: true,
  minWidth: "150px",
}

const upperlinkCommColumn = {
  name: "Uppperlink Comm",
  selector: "amount",
  format: row => formatCurrency(getCommissionForAmount(row.token, 0.15)),
  sortable: true,
  minWidth: "150px"
}

const transactionRefColumn = {
  name: "Transaction Ref",
  selector: "reference",
  sortable: true,
  minWidth: "150px",
  formatExport: workaroundExcel
}

export const resellerColumns = [
  {
    name: "S/N",
    selector: "__serialNumber",
    format: row => !!row.__serialNumber ? `${row.__serialNumber}.` : ''
  },
  {
    name: "Meter Number",
    selector: "customerMeterID",
    sortable: true,
    grow: 3,
    minWidth: "150px",
    formatExport: workaroundExcel
  },
  {
    name: "Token",
    selector: "value",
    grow: 3,
    minWidth: "200px",
    wrap: true,
    formatExport: workaroundExcel
  },
  {
    name: "Amount",
    selector: "amount",
    format: row => formatCurrency(row.token),
    sortable: true,
    minWidth: "150px"
  },
  {...firstBankCommColumn},
  {...upperlinkCommColumn},
  {
    name: "Comm. Amount",
    selector: "amount",
    format: row => formatCurrency(getCommissionForAmount(row.token, row.resellerPercent)),
    sortable: true,
    minWidth: "150px"
  },
  {
    name: "Net Amount",
    selector: "amount",
    format: row => row.token ? formatCurrency(parseFloat(row.token) - getCommissionForAmount(row.token, row.resellerPercent)) : '',
    sortable: true,
    minWidth: "150px"
  },
  {...transactionRefColumn},
  {
    name: "Date",
    selector: "date",
    sortable: true,
    format: row => row.date ? moment(row.date).format("lll") : '',
    wrap: true,
    grow: 3,
    minWidth: "200px"
  },
  {
    name: "Customer Type",
    selector: "accountType",
    format: row => getAccountTypeName(row.accountType),
    sortable: true,
    minWidth: "150px"
  },
  // {
  //   name: "Reference",
  //   selector: "refNumber",
  //   sortable: true,
  //   grow: 3,
  //   minWidth: "150px"
  // },
  // {
  //   name: "Customer Name",
  //   selector: "customerName",
  //   sortable: true,
  //   grow: 3,
  //   wrap: true,
  //   minWidth: "150px"
  // },
  // {
  //   name: "Customer Address",
  //   selector: "customerAddress",
  //   grow: 3,
  //   wrap: true,
  //   minWidth: "200px"
  // },
  // {
  //   name: "Kilowatts",
  //   selector: "units",
  //   sortable: true
  // },
  // {
  //   name: "Description",
  //   selector: "description",
  //   grow: 5,
  //   wrap: true,
  //   minWidth: "250px"
  // },
  // {
  //   name: "Status",
  //   selector: "status",
  //   grow: 2,
  //   minWidth: "200px"
  // }
  // {
  //   cell: row => (
  //     <PermissionGuard permissions={["GET_TRANSACTION"]}>
  //       {({ show }) =>
  //         show && (
  //           <Button
  //             tag={Link}
  //             to={`/app/transactions/${row.transactionId}`}
  //             pill
  //           >
  //             View
  //           </Button>
  //         )
  //       }
  //     </PermissionGuard>
  //   ),
  //   ignoreRowClick: true,
  //   allowOverflow: true,
  //   button: true
  // }
];

export const superAdminColumns = [
  {
    name: "S/N",
    selector: "__serialNumber",
    format: row => !!row.__serialNumber ? `${row.__serialNumber}.` : ''
  },
  {
    name: "Meter Number",
    selector: "customerMeterID",
    sortable: true,
    grow: 3,
    minWidth: "!50px",
    formatExport: workaroundExcel
  },
  {
    name: "Token",
    selector: "value",
    grow: 3,
    minWidth: "200px",
    wrap: true,
    formatExport: workaroundExcel
  },
  {
    name: "Amount",
    selector: "amount",
    format: row => formatCurrency(row.token),
    sortable: true,
    minWidth: "150px"
  },
  {...firstBankCommColumn},
  {...upperlinkCommColumn},
  {
    name: "Comm. Amount",
    selector: "amount",
    format: row => formatCurrency(getCommissionForAmount(row.token, row.resellerPercent)),
    sortable: true,
    minWidth: "150px"
  },
  {
    name: "Kestavar Comm.",
    selector: "amount",
    format: row => formatCurrency(getKestavarCommissionForAmount(row.token, row.resellerPercent))
  },
  {
    name: "Net Amount",
    selector: "amount",
    format: row => row.token ? formatCurrency(parseFloat(row.token) - getCommissionForAmount(row.token, row.resellerPercent)) : '',
    sortable: true,
    minWidth: "150px"
  },
  {...transactionRefColumn},
  {
    name: "Date",
    selector: "date",
    sortable: true,
    format: row => row.date ? moment(row.date).format("lll") : '',
    wrap: true,
    grow: 3,
    minWidth: "200px"
  },
  {
    name: "Customer Type",
    selector: "accountType",
    format: row => getAccountTypeName(row.accountType),
    sortable: true,
    minWidth: "150px"
  },
]