import { removeAuthToken, removeUserData, getUserData } from "./utils/auth";

const userRoleMigration = () => {
  const user = getUserData()
  if (!user) return
  // User previously used the old user schema
  if (!!user.roleId) {
    // Force a re-login so up-to date user data can be fetched
    removeAuthToken()
    removeUserData()
  }
} 
export const performMigrations = () => {
  userRoleMigration()
}