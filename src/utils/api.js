import { connect } from 'react-refetch'
import urlJoin from 'url-join'

import { getAuthToken } from './auth'

export const API_URL = process.env.REACT_APP_API_URL

export const API_V1_URL = `${API_URL}/v1`

export const API_ACTIVE_URL = API_V1_URL

// Some error responses swap the code and message, correct them here
const SWAPPED_ERRORS = ['108']

export const correctErrorResponse = res => {
  if (SWAPPED_ERRORS.some(errCode => res.message === errCode)) {
    return {
      responseCode: res.message,
      message: res.responseCode,
    }
  } else {
    return res
  }
}

const newError = cause => {
  const e = new Error(cause.message || cause)
  e.cause = cause
  return e
}

export const refetch = connect.defaults({
  buildRequest: mapping => {
    const options = {
      ...mapping,
      headers: {
        ...mapping.headers,
        Authorization: `Bearer ${getAuthToken()}`,
      },
    }

    return new Request(urlJoin(API_V1_URL, mapping.url), options)
  },
  handleResponse: response => {
    if (
      response.headers.get('content-length') === '0' ||
      response.status === 204
    ) {
      return
    }

    // Don't bother to parse response body for 401 Authorized
    // It doesn't have any
    if (response.status === 401) {
      return Promise.reject({
        message: 'Unauthorized to perform action.',
      })
    }

    const json = response.json() // TODO: support other response types
    if (response.status >= 200 && response.status < 300) {
      // TODO: support custom acceptable statuses
      // Sometimes, we get 200 OK with no body content
      // and that makes response.json() fail, ignore that
      return json.catch(err => {
        if (process.env.NODE_ENV === 'development') console.warn(err)
        return Promise.resolve({})
      })
    } else {
      return json.then(cause =>
        Promise.reject(newError(correctErrorResponse(cause)))
      )
    }
  },
})
