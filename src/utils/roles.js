export const AuthorizationType = {
  all: 'ALL',
  oneOrMore: 'ONE_ORE_MORE',
}

const allPerms = userPermissions => (acc, perm) =>
  acc && userPermissions.includes(perm)
const oneOrMorePerms = userPermissions => (acc, perm) =>
  acc || userPermissions.includes(perm)

const AuthorizationStrategies = {
  [AuthorizationType.all]: {
    reducer: allPerms,
    startValue: true
  },
  [AuthorizationType.oneOrMore]: {
    reducer: oneOrMorePerms,
    startValue: false
  }
}

export const isAuthorized = (
  userPermissions,
  requiredPermissions,
  type = AuthorizationType.all
) => {
  if (!AuthorizationStrategies[type]) {
    throw new Error(`Unknown authorization strategy ${type}`)
  }
  if (userPermissions.includes('ALL')) return true
  const allFound = requiredPermissions.reduce(
    AuthorizationStrategies[type].reducer(userPermissions),
    AuthorizationStrategies[type].startValue
  )
  return allFound
}

export const isSuperAdmin = userPermissions =>
  userPermissions.includes('ALL')

export const isReseller = userPermissions => !isSuperAdmin(userPermissions)