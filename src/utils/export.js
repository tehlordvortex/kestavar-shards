import pdfMake from "pdfmake";
import { isVisibleColumn, getColumnTitle, getColumnTextForRow } from "./table";
import pdfFonts from "pdfmake/build/vfs_fonts";
import * as stringify from "csv-stringify"
import { generateFilterQueryString } from "./generateFilterQueryString"
import { getAuthToken } from "./auth"
import axios from "axios"
import { API_ACTIVE_URL } from "./api"
import { enhanceResults } from "./enhanceResults"

pdfMake.vfs = pdfFonts.pdfMake.vfs;

const stringifyPromise = (...args) => new Promise((resolve, reject) => {
  stringify(...args, (err, data) => {
    if (err) reject(err)
    else resolve(data)
  })
})

const fetchAllTransactions = ({ totalCount, filter, reseller }) => {
  const filters = generateFilterQueryString(filter)
  const authToken = getAuthToken()
  const fetchUrl = `/transactions?offset=0&limit=${totalCount}${filters}`;
  return axios.get(`${API_ACTIVE_URL}${fetchUrl}`, {
    headers: {
      Authorization: `Bearer ${authToken}`
    }
  })
    .then(({ data }) => {
      const results = data[0]
      const totalAmount = data[2]
      return enhanceResults({ results, totalAmount, reseller })
    })
}

export const exportAsCSV = async ({
  columns,
  totalCount,
  filter,
  reseller,
  notificationControl
}) => {
  const id = notificationControl.addNotification({
    title: 'Preparing CSV Export',
    message: 'Please wait while your export is prepared...',
    dismissAfter: false
  })
  try {
    const data = await fetchAllTransactions({ totalCount, filter, reseller })
    // first, the headers
    const headerRow = columns
      .filter(isVisibleColumn)
      .map(getColumnTitle)
    const lines = data.map(datum => {
      return columns
        .filter(isVisibleColumn)
        .map(col => getColumnTextForRow(col, datum))
        .map(col => col.replace(/₦/g, ""))
    });
    const output = await stringifyPromise([ headerRow, ...lines ])
    const exportDateTime = new Date();
    const filename = `kevastar-transactions-${exportDateTime.toLocaleString()}.csv`;
    const downloadTrigger = document.createElement("a");
    downloadTrigger.style.display = "none";
    downloadTrigger.setAttribute("href", `data:text/csv;charset=utf-8,${output}`);
    downloadTrigger.setAttribute("download", filename);
    document.body.appendChild(downloadTrigger);
    downloadTrigger.click();
    document.body.removeChild(downloadTrigger);
    notificationControl.addNotification({
      title: 'Exported successfully',
      message: `Exported to ${filename}`,
      dismissAfter: 3000
    })
  } catch(e) {
    notificationControl.addNotification({
      title: 'Failed to export',
      message: `An error occured while processing the export. Please try again later.`,
      dismissAfter: 3000
    })
    if (process.env.NODE_ENV === "development") console.warn(e)
  } finally {
    notificationControl.removeNotification(id)
  }
};
export const exportAsPDF = async ({ columns, totalCount, filter, reseller, notificationControl }) => {
  const id = notificationControl.addNotification({
    title: 'Preparing PDF Export',
    message: 'Please wait while your export is prepared...',
    dismissAfter: false
  })
  try {
    const data = await fetchAllTransactions({ totalCount, filter, reseller })
    const exportDateTime = new Date();
    const docDefinition = {
      info: {
        title: `Kevastar Transactions Export`,
        author: `Kevastar Dashboard`
      },
      pageOrientation: 'landscape',
      footer: { text: `Generated on ${exportDateTime.toLocaleString()}`, style: "subheading" },
      content: [
        { text: "Kevastar Transactions", style: "heading" },
        { text: `Generated on ${exportDateTime.toLocaleString()}`, style: "subheading" },
        {
          layout: "lightHorizontalLines",
          table: {
            headerRows: 1,
            body: [
              columns.filter(isVisibleColumn).map(getColumnTitle),
              ...data.map(datum => columns
                .filter(isVisibleColumn)
                .map(col => getColumnTextForRow(col, datum)))
            ]
          }
        }
      ],
      styles: {
        header: {
          fontSize: 25,
          bold: true,
          margin: [0, 0, 0, 5]
        },
        subheading: {
          fontSize: 10,
          margin: [0, 0, 0, 25],
          italics: true,
          color: 'gray'
        },
        footer: {
          fontSize: 8,
          margin: [0, 15, 15, 25],
          italics: true,
          color: 'gray'
        }
      },
      pageMargins: [15, 15]
    };
    const filename = `kevastar-transactions-${exportDateTime.toLocaleString()}.pdf`
    pdfMake.createPdf(docDefinition).download(filename);
    notificationControl.addNotification({
      title: 'Exported successfully',
      message: `Exported to ${filename}`,
      dismissAfter: 3000
    })
  } catch(e) {
    notificationControl.addNotification({
      title: 'Failed to export',
      message: `An error occured while processing the export. Please try again later.`,
      dismissAfter: 3000
    })
    if (process.env.NODE_ENV === "development") console.warn(e)
  } finally {
    notificationControl.removeNotification(id)
  }
};
