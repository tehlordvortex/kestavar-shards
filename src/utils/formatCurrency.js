export const formatCurrency = num =>
  num !== null && num !== undefined
    ? parseFloat(num).toLocaleString("en-NG", {
        style: "currency",
        currency: "NGN"
      })
    : "";
