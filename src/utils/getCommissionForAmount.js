import { KESTAVAR_COMMISSION_PERCENTAGE } from "./constants";

const convertToDecmial = precentage => precentage / 100
export const getCommissionForAmount = (amount, resellerPercent) => {
  if (isNaN(amount) || isNaN(resellerPercent)) {
    return NaN;
  }
  const precentage = convertToDecmial(parseFloat(resellerPercent))
  return precentage * parseFloat(amount)
}

export const getKestavarCommissionForAmount = (amount, resellerPercent) => {
  const kestavarPercent = KESTAVAR_COMMISSION_PERCENTAGE - resellerPercent
  return getCommissionForAmount(amount, kestavarPercent)
}