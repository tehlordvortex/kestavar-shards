export const accountTypes = [
  {
    value: '1',
    label: 'Prepaid',
  },
  {
    value: '2',
    label: 'Postpaid',
  },
]

export const userAccountTypes = [
  {
    value: '0',
    label: 'Postpaid'
  },
  {
    value: '1',
    label: 'Prepaid'
  }
]

export const merchantFKs = [
  {
    value: '1',
    label: 'Eko Electricity Distribution Plc',
  },
  {
    value: '2',
    label: 'Ikeja Electric',
  },
  {
    value: '3',
    label: 'Enugu Electricity Distribution Company',
  },
  {
    value: '4',
    label: 'Ibadan Electricity Distribution Company',
  },
  {
    value: '5',
    label: 'Kano Electricity Distribution Company',
  },
  {
    value: '6',
    label: 'Yola Electricity Distribution Company',
  },
  {
    value: '7',
    label: 'Benin Electricity Distribution Company',
  },
  {
    value: '8',
    label: 'PH Electricity Distribution Company',
  },
  {
    value: '9',
    label: 'Kaduna Electric',
  },
  {
    value: '10',
    label: 'Abuja Electricity Distribution Company',
  },
  {
    value: '11',
    label: 'Jos Electricity Distribution Company',
  },
]

export const statusTypes = [
  {
    value: '1',
    label: 'Successful',
    class: 'success'
  },
  {
    value: '2',
    label: 'All',
    class: 'primary'
  },
  {
    value: '3',
    label: 'Failed',
    class: 'danger'
  },
  {
    value: '4',
    label:'Pending',
    class: 'primary'
  }
]

export const KESTAVAR_COMMISSION_PERCENTAGE = 2