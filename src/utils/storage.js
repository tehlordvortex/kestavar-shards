export const notifyStorageChange = () => {
  const event = new Event('localStorageChange')
  document.dispatchEvent(event)
}