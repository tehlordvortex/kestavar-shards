import { accountTypes } from "./constants";

export const getAccountTypeName = type => {
  if (!type) {
    return ""
  }
  const accountType = accountTypes.find(accountType => accountType.value === type.toString())
  return accountType ? accountType.label : "";
}
