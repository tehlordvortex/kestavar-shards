export const enhanceResults = ({
	results,
	totalAmount,
	reseller
}) => {
	if (results && results.length > 0) {
    const filteredResults = reseller ?
      results.filter(result => result.userId === reseller)
      : results
    const resultsWithSerial = filteredResults.map((result, index) => ({
      ...result,
      __serialNumber: index + 1
    }))
    const resellerPercent = results.length > 0 ?
      results[0].resellerPercent
      : 0
    return [...resultsWithSerial, {
      __serialNumber: "Total",
      token: totalAmount,
      resellerPercent
    }]
  } else {
    return []
  }
}