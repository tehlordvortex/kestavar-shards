export const isVisibleColumn = column => !!column.name;
const hasFormatter = column => !!column.format;
const hasExportFormatter = column => !!column.formatExport;
export const getColumnTitle = column => column.name;
export const getColumnTextForRow = (column, row) => {
  let value = "";
  if (hasFormatter(column)) {
    value = column.format(row);
  }
  else {
    value = row[column.selector];
  }
  value = value || ""
  if (hasExportFormatter(column)) {
  	value = column.formatExport(value)
  }
  return value
};
export const getAmount = row => row.token