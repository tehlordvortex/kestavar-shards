import { notifyStorageChange } from "./storage";

export const getAuthToken = () => {
  return localStorage.getItem("token") || "";
};

export const hasAuthToken = () => {
  return !!localStorage.getItem("token");
};

export const setAuthToken = token => {
  localStorage.setItem("token", token);
  notifyStorageChange()
};

export const removeAuthToken = () => {
  localStorage.removeItem("token");
  notifyStorageChange()
};

export const getUserData = () => {
  const user = localStorage.getItem("user");
  return user ? JSON.parse(user) : "";
};

export const setUserData = user => {
  localStorage.setItem("user", JSON.stringify(user));
  notifyStorageChange()
};

export const removeUserData = () => {
  localStorage.removeItem("user");
  notifyStorageChange()
};

export const logoutUser = (history) => {
  removeAuthToken()
  removeUserData()
  history.push("/auth/login")
}
