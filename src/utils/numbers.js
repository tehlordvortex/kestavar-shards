export const isValidNumber = num => num !== 0 && !!num && !isNaN(num);
export const sum = (acc, val) => acc + val;
export const toNumber = num => parseFloat(num);
