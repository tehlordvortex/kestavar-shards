export const generateFilterQueryString = filter => {
  const hasFilter = !!filter
  const hasSearch = hasFilter && !!filter.search;
  const search = filter && filter.search
  const searchQuery = hasSearch ? `&search=${search}` : "";
  const dateFilterQuery = hasFilter && !!filter.startDate && !!filter.endDate
    ? `&startDate=${filter.startDate.toLocaleDateString()}&endDate=${filter.endDate.toLocaleDateString()}`
    : "";
  const hasStatusFilter = hasFilter && !!filter.status;
  const statusFilterQuery = hasStatusFilter ? `&filter=${filter.status}` : '';
  const filters = `${searchQuery}${dateFilterQuery}${statusFilterQuery}`
  return filters
}