import React, { useEffect, useMemo, useState } from "react";
import PropTypes from "prop-types";
import { PromiseState } from "react-refetch";

import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Alert,
  Button,
  FormInput,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  ButtonGroup,
  Form,
  FormRadio,
  PopoverBody,
  PopoverHeader,
  Dropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
  FormSelect
} from "shards-react";
import DataTable from "react-data-table-component";

import { Formik } from "formik";
import { object, date, string, mixed } from "yup";

import { refetch } from "../utils/api";

import Spinner from "../components/common/Spinner";
import PageTitle from "../components/common/PageTitle";
import { DatePickerField } from "../components/common/DatePickerField";
import { resellerColumns, superAdminColumns } from "../data/columns";
import { exportAsCSV, exportAsPDF } from "../utils/export";

import { useUser } from '../contexts/user'
import { isSuperAdmin } from "../utils/roles";
import { statusTypes } from "../utils/constants";

import PermissionGuard from '../components/common/PermissionGuard'
import Popover from "react-popover"

import { generateFilterQueryString } from "../utils/generateFilterQueryString"
import { enhanceResults } from "../utils/enhanceResults"
import { useNotifications } from "../components/common/Notifications"

const TransactionFilterSchema = object().shape({
  search: string(),
  startDate: mixed().oneOf([date(), null]),
  endDate: mixed().oneOf([date(), null]).when('startDate', (startDate, schema) => {
    !!startDate ? date.required() : mixed().oneOf([date(), null])
  }),
  status: mixed().oneOf(statusTypes.map(type => type.value))
})

const FilterPopover = ({ filter, applyFilters, resellersFetch }) => {
  const [visible, setVisible] = useState(false)
  const togglePopover = () => {
    setVisible(!visible)
  }
  const closeAndApply = (values) => {
    setVisible(false)
    applyFilters(values)
  }
  const closeOnly = () => {
    setVisible(false)
  }
  const popoverProps = {
    isOpen: visible,
    place: "below",
    onOuterAction: closeOnly
  }
  return (
    <Popover
      {...popoverProps}
      body={(
        <div className="rounded border">
          <PopoverHeader>Filter</PopoverHeader>
          <PopoverBody className="bg-white">
            <Formik
              initialValues={{
                ...filter
              }}
              onSubmit={values => closeAndApply(values)}
              onReset={values => closeAndApply(values)}
              validationSchema={TransactionFilterSchema}
              enableReinitialize
            >
              {({
                values,
                errors,
                touched,
                setFieldValue,
                handleChange,
                handleBlur,
                handleSubmit,
                handleReset
              }) => (
                <Form onSubmit={handleSubmit} onReset={handleReset}>
                  <Row className="mb-2">
                    <Col>
                      <InputGroup>
                        <InputGroupAddon type="prepend">
                          <InputGroupText>
                            <i className="material-icons">search</i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <FormInput
                          type="text"
                          name="search"
                          value={values.search}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          placeholder="Meter Number or Reference"
                          invalid={errors.search && touched.search}
                        />
                      </InputGroup>
                    </Col>
                  </Row>
                  <Row className="mb-2">
                    <Col className="mb-1">
                      <DatePickerField
                        name="startDate"
                        value={values.startDate}
                        onChange={setFieldValue}
                        invalid={touched.startDate && errors.startDate}
                        placeholder="Select start date..."
                        selectsStart
                        startDate={values.startDate}
                        endDate={values.endDate}
                      />
                    </Col>
                    <Col xs={12}>
                      <DatePickerField
                        name="endDate"
                        value={values.endDate}
                        onChange={setFieldValue}
                        invalid={touched.endDate && errors.endDate}
                        placeholder="Select end date..."
                        selectsEnd
                        startDate={values.startDate}
                        endDate={values.endDate}
                      />
                    </Col>
                  </Row>
                  <Row className="mb-2">
                    <Col>
                      <small className="text-muted mb-1">Status:</small>
                      {statusTypes.map(type => (
                        <FormRadio
                          name="status"
                          checked={values.status === type.value}
                          onChange={() => {
                            setFieldValue("status", type.value)
                          }}
                          onBlur={handleBlur}
                          key={type.value}
                        >
                          {type.label}
                        </FormRadio>
                      ))}
                    </Col>
                  </Row>
                  <PermissionGuard permissions={['ALL']}>
                    {({ show }) => show && (
                      <Row className="mb-2">
                        <Col>
                          <small className="text-muted mb-1">Reseller:</small>
                          {resellersFetch.fulfilled && (
                            <FormSelect
                              name="reseller"
                              value={values.reseller}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              invalid={errors.reseller && touched.reseller}
                            >
                              {[
                                ...resellersFetch.value.map(reseller => (
                                    <option
                                      key={reseller.userModelId}
                                      value={reseller.userModelId}
                                    >
                                      {reseller.fullName}
                                    </option>
                                  )),
                                <option key="all" value="">All</option>
                              ]}
                            </FormSelect>
                          )}
                          {resellersFetch.pending && (
                            <p>Loading...</p>
                          )}
                        </Col>
                      </Row>
                    )}
                  </PermissionGuard>
                  <Row>
                    <Col>
                      <Button block type="submit">Apply</Button>
                    </Col>
                    <Col>
                      <Button outline block theme="secondary" type="reset">Reset</Button>
                    </Col>
                  </Row>
                </Form>
              )}
            </Formik>
          </PopoverBody>
        </div>
      )}
    >
      <Button outline onClick={togglePopover}>
        <i id="popover-filter" className="material-icons">filter_list</i> Filter
      </Button>
    </Popover>
  )
}

const RefetchedFilterPopover = refetch(() => ({
  resellersFetch: '/users'
}))(FilterPopover)

const ExportDropdown = ({ results, columns, filter, totalCount, reseller, disabled }) => {
  const [open, setOpen] = useState(false)
  const { addNotification, removeNotification } = useNotifications()
  const notificationControl = { addNotification, removeNotification }
  const toggle = () => {
    setOpen(!open)
  }
  const exportCSV = () => {
    exportAsCSV({
      columns,
      totalCount,
      filter,
      reseller,
      notificationControl
    })
  }
  const exportPDF = () => {
    exportAsPDF({
      columns,
      totalCount,
      filter,
      reseller,
      notificationControl
    })
  }
  return (
    <Dropdown className="ml-3" disabled={disabled} open={open} toggle={toggle}>
      <DropdownToggle>
        <i className="material-icons text-white">import_export</i>
        Export
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem onClick={exportCSV}>
          Export As CSV
        </DropdownItem>
        <DropdownItem onClick={exportPDF}>
          Export as PDF
        </DropdownItem>
      </DropdownMenu>
    </Dropdown>
  )
}

const useEnhancedResults = ({
  results,
  totalAmount,
  reseller
}) => {
  return useMemo(() => {
    return enhanceResults({ results, totalAmount, reseller })
  }, [results, totalAmount])
}

const useAppropriateColumnsForRole = ({ superAdminColumns, resellerColumns }) => {
  const user = useUser()
  return useMemo(() => {
    if (isSuperAdmin(user.permissionsArray)) {
      return superAdminColumns
    } else {
      return resellerColumns
    }
  }, [user, superAdminColumns, resellerColumns])
}

const Transactions = ({ transactionsFetch, getPage }) => {
  let [results, totalCount, totalAmount] = [];
  const [rowsPerPage, setRowsPerPage] = React.useState(25);
  const [currentPage, setCurrentPage] = React.useState(1);
  const [filter, setFilter] = React.useState({
    search: '',
    startDate: null,
    endDate: null,
    status: statusTypes[0].value,
    reseller: ''
  });
  const disableButtons =
    transactionsFetch.pending || transactionsFetch.rejected;
  // const hasFilter = filter && filter.startDate && filter.endDate;
  // const hasSearch = !!search;
  let offset = (currentPage - 1) * rowsPerPage
  if (totalCount) {
    // offset cannot be larger than total result count - items per page
    offset = Math.min(totalCount - rowsPerPage, offset)
  }
  // offset cannot be smaller than zero
  offset = Math.max(offset, 0)
  const fetchActivePage = () => {
    getPage({
      offset,
      limit: rowsPerPage,
      filter,
    });
  };
  useEffect(() => {
    fetchActivePage();
  }, [rowsPerPage, currentPage, filter]);
  const handlePageChange = page => {
    setCurrentPage(page);
  };
  const handleRowsPerPageChange = rows => {
    setRowsPerPage(rows);
  };
  if (transactionsFetch.fulfilled) {
    [results, totalCount, totalAmount] = transactionsFetch.value;
  }
  const enhancedResults = useEnhancedResults({ results, totalAmount, reseller: filter.reseller })
  const columns = useAppropriateColumnsForRole({ superAdminColumns, resellerColumns })
  return (
    <Container fluid className="main-content-container px-5">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="12"
          title="All Transactions"
          subtitle="Transactions"
          className="text-sm-left"
        />
      </Row>

      <Row className="py-3">
        <Col>
          <Card>
            <CardHeader className="border-bottom">
              <div className="row d-flex justify-content-between align-items-center">
                <div className="col-12 col-md-3 mb-3 mb-md-0">
                  <h5 className="mb-0">Transaction List</h5>
                </div>
                <div className="col-12 col-md-4 d-flex justify-content-center justify-content-md-end">
                  <RefetchedFilterPopover filter={filter} applyFilters={setFilter} />
                  <ExportDropdown
                    disabled={disableButtons}
                    results={enhancedResults}
                    filter={filter}
                    columns={columns}
                    totalCount={totalCount}
                    reseller={(filter && filter.reseller) || undefined}
                  />
                </div>
              </div>
            </CardHeader>
            <CardBody>
              {transactionsFetch.rejected && (
                <Alert theme="danger">{transactionsFetch.reason.message}</Alert>
              )}
              <DataTable
                noHeader
                data={enhancedResults}
                columns={columns}
                keyField="transactionId"
                progressComponent={<Spinner className="my-1" />}
                progressPending={transactionsFetch.pending}
                pagination
                paginationServer
                paginationPerPage={rowsPerPage}
                paginationTotalRows={totalCount}
                paginationRowsPerPageOptions={[25,50,75,100]}
                onChangePage={handlePageChange}
                onChangeRowsPerPage={handleRowsPerPageChange}
              />
              <small className="text-muted">Don't see anything? Check your filters. Or check the next page.</small>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

Transactions.propTypes = {
  transactionsFetch: PropTypes.instanceOf(PromiseState).isRequired,
  getPage: PropTypes.func.isRequired
};

export default refetch(props => ({
  transactionsFetch: "/transactions?offset=0&limit=10",
  getPage: ({ offset, limit, filter }) => {
    const filters = generateFilterQueryString(filter)
    const fetchUrl = `/transactions?offset=${offset}&limit=${limit}${filters}`;
    return {
      transactionsFetch: {
        url: fetchUrl,
        force: true,
      }
    };
  }
}))(Transactions);
