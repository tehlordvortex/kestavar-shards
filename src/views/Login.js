import React from "react";
import { Card, CardBody, Form, FormInput, Button, Alert } from "shards-react";
import { Formik } from "formik";
import { object, string } from "yup";
import axios from "axios";

import { API_URL } from "../utils/api";
import { setAuthToken, setUserData } from "../utils/auth";

const LoginSchema = object().shape({
  email: string()
    .email("Invalid email")
    .required("Required"),
  password: string().required("Required")
});

export default class Login extends React.Component {
  state = {
    error: ""
  };

  _login = (values, { setSubmitting }) => {
    const { history } = this.props;
    setSubmitting(true);
    axios
      .post(`${API_URL}/account/login`, {
        Email: values.email,
        Password: values.password
      })
      .then(res => {
        const response = res.data;
        const { token, user } = response;
        setAuthToken(token);
        setUserData(user);
        this.setState({
          error: ""
        });
        history.push("/app");
      })
      .catch(err => {
        if (process.env.NODE_ENV === "development") console.warn(err);
        if (err.response && err.response.data) {
          this.setState({
            error: err.response.data
          });
        } else {
          this.setState({
            error: err.message
          });
        }
      })
      .finally(() => {
        setSubmitting(false);
      });
  };

  render() {
    const { error } = this.state;
    return (
      <>
        <h3 className="mb-4">Login to Kestavar</h3>
        <Card className="login-card">
          <CardBody>
            {error && <Alert theme="danger">{error}</Alert>}
            <Formik
              initialValues={{ email: "", password: "" }}
              validationSchema={LoginSchema}
              onSubmit={this._login}
            >
              {({
                values,
                errors,
                touched,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit
              }) => (
                <Form className="login-form">
                  <FormInput
                    type="email"
                    name="email"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder="Enter your email"
                    className="mb-3"
                    invalid={errors.email && touched.email}
                  />
                  <FormInput
                    type="password"
                    name="password"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    placeholder="Enter your password"
                    className="mb-4"
                    invalid={errors.password && touched.password}
                  />
                  <div className="d-flex justify-content-end">
                    <Button
                      type="submit"
                      pill
                      onClick={handleSubmit}
                      disabled={isSubmitting}
                    >
                      {isSubmitting ? (
                        <>
                          <span
                            className="spinner-border spinner-border-sm"
                            role="status"
                            aria-hidden="true"
                          />
                          <span className="sr-only">Loading...</span>
                        </>
                      ) : (
                        "Login"
                      )}
                    </Button>
                  </div>
                </Form>
              )}
            </Formik>
          </CardBody>
        </Card>
      </>
    );
  }
}
