import React from "react";
import { Container, Button } from "shards-react";

const Error401 = ({ history }) => (
  <Container fluid className="main-content-container px-4 pb-4">
    <div className="error">
      <div className="error__content">
        <h2>401</h2>
        <h3>You can't go there!</h3>
        <p>You are not authorized to view this page.</p>
        <Button onClick={() => history.go(-1)} pill>&larr; Go Back</Button>
      </div>
    </div>
  </Container>
);

export default Error401;