import React from 'react'
import moment from 'moment'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Alert,
} from 'shards-react'

import { refetch } from '../utils/api'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'
import { formatCurrency } from '../utils/formatCurrency';

const formatDate = date => moment(date).format('lll')

const Transaction = ({ transactionFetch }) => {
  const transaction = transactionFetch.value
  return (
    <Container fluid className="main-content-container px-5">
      {/* Page Header */}
      <Row noGutters className="page-header py-4 align-items-center">
        <PageTitle
          sm="11"
          title="View Transaction"
          subtitle="Transactions"
          className="text-sm-left"
        />
        <Col>{transactionFetch.pending && <Spinner />}</Col>
      </Row>

      <Row className="py-3 flex-column-reverse flex-md-row">
        <Col md="7">
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between align-items-center">
                <h5 className="mb-0">Transaction Details</h5>
              </div>
            </CardHeader>
            <CardBody>
              {transactionFetch.rejected && (
                <Alert theme="danger">{transactionFetch.reason.message}</Alert>
              )}
              {transactionFetch.fulfilled && (
                <>
                  <h6 className="text-muted">Summary</h6>
                  <small className="text-muted d-block my-2">
                    {formatDate(transaction.date)}
                  </small>
                  <p className="text-muted mb-0">
                    {(transaction.orderNumber && `Order ${transaction.orderNumber}`) || 'No order number provided'}
                  </p>
                  <h2 className="mb-2">
                    {(transaction.units &&
                      `${transaction.units} ${transaction.unitsType}`) ||
                      'No units provided'}
                  </h2>
                  <h4 className="mb-2">
                    {(transaction.tokenString && formatCurrency(transaction.tokenString))}
                  </h4>
                  <h6 className={transaction.tax ? 'mb-0' : 'mb-3'}>
                    {(transaction.amount && `${formatCurrency(transaction.amount)}`) ||
                      'No amount provided'}
                  </h6>
                  {(transaction.tax && <small className="d-block text-muted mb-3">({formatCurrency(transaction.tax)} tax)</small>)}
                  <p>
                    {transaction.description || 'No description provided'}
                  </p>
                  <h6 className="text-muted">Other Details</h6>
                  <small className="text-muted text-uppercase">
                    Receipt Number
                  </small>
                  <p className="mb-3">
                    {transaction.receiptNumber || 'No receipt number provided'}
                  </p>
                  <small className="text-muted text-uppercase">Tariff</small>
                  <p className="mb-3">
                    {transaction.tarrif || 'No tariff provided'}
                  </p>
                  <small className="text-muted text-uppercase">Value</small>
                  <p className="mb-3">
                    {transaction.value || 'No value provided'}
                  </p>
                  <small className="text-muted text-uppercase">Value 1</small>
                  <p className="mb-3">
                    {transaction.value1 || 'No value 1 provided'}
                  </p>
                  <small className="text-muted text-uppercase">
                    Biller Status
                  </small>
                  <p className="mb-3">
                    {transaction.billerStatus || 'No biller status provided'}
                  </p>
                </>
              )}
            </CardBody>
          </Card>
        </Col>
        <Col md={{ offset: 1, size: 4 }}>
          <Card className="mb-4 mb-md-0">
            <CardHeader>
              <h5 className="mb-0">Customer</h5>
            </CardHeader>
            <CardBody>
              {transactionFetch.fulfilled && (
                <>
                  <h4>{transaction.customerName || 'No name provided'}</h4>
                  <p>{transaction.customerAddress || 'No address provided'}</p>
                </>
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default refetch(({ match }) => ({
  transactionFetch: `/transaction/${match.params.id}`,
}))(Transaction)
