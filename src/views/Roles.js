import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Alert,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'shards-react'
import { Link } from 'react-router-dom'
import DataTable from 'react-data-table-component'

import { refetch } from '../utils/api'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'
import { PromiseState } from 'react-refetch'
import PermissionGuard from '../components/common/PermissionGuard'

const Roles = ({ rolesFetch, deleteRole, deleteRoleResponse }) => {
  const [selectedRole, setSelectedRole] = useState(null)
  React.useEffect(() => {
    if (deleteRoleResponse && deleteRoleResponse.fulfilled) {
      setSelectedRole(null)
    }
  }, [deleteRoleResponse])
  const promptDeleteRole = role => {
    setSelectedRole(role)
  }
  const toggle = () => {
    setSelectedRole(null)
  }
  const columns = [
    {
      name: 'Name',
      selector: 'roleName',
    },
    {
      cell: row => (
        <div className="d-flex">
          <PermissionGuard permissions={['CREATE_ROLE']}>
            {({ show }) =>
              show && (
                <Button
                  pill
                  className="mr-2"
                  tag={Link}
                  to={`/app/roles/${row.roleModelId}/edit`}
                >
                  <i className="material-icons">edit</i>
                </Button>
              )
            }
          </PermissionGuard>
          <PermissionGuard permissions={['REMOVE_ROLE']}>
            {({ show }) =>
              show && (
                <Button
                  onClick={() => promptDeleteRole(row)}
                  pill
                  outline
                  theme="danger"
                >
                  <i className="material-icons">delete</i>
                </Button>
              )
            }
          </PermissionGuard>
        </div>
      ),
      allowOverflow: true,
      ignoreRowClick: true,
    },
  ]
  const isDeleting = deleteRoleResponse && deleteRoleResponse.pending
  const hasError = deleteRoleResponse && deleteRoleResponse.rejected
  const errorReason = hasError && deleteRoleResponse.reason
  return (
    <Container fluid className="main-content-container px-5">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="12"
          title="All Roles"
          subtitle="Roles"
          className="text-sm-left"
        />
      </Row>

      <Row className="py-3 justify-content-center">
        <Col md="8" lg="5">
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between align-items-center">
                <h5 className="mb-0">Role List</h5>
                {rolesFetch.pending && <Spinner />}
                {!rolesFetch.pending && (
                  <Button pill tag={Link} to="/app/roles/create">
                    Create role
                  </Button>
                )}
              </div>
            </CardHeader>
            <CardBody>
              {rolesFetch.rejected && (
                <Alert theme="danger">{rolesFetch.reason.message}</Alert>
              )}
              {rolesFetch.fulfilled && (
                <DataTable noHeader data={rolesFetch.value} columns={columns} />
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Modal size="sm" centered open={!!selectedRole} toggle={toggle}>
        <ModalHeader>Delete role</ModalHeader>
        <ModalBody>
          {hasError && <Alert theme="danger">{errorReason.message}</Alert>}
          {!!selectedRole &&
            `Are you sure you want to delete ${selectedRole.roleName}?`}
        </ModalBody>
        <ModalFooter>
          <Button onClick={toggle} outline pill>
            Cancel
          </Button>
          <Button
            onClick={() => deleteRole(selectedRole)}
            disabled={isDeleting}
            pill
            theme="danger"
          >
            {isDeleting ? <Spinner /> : 'Delete'}
          </Button>
        </ModalFooter>
      </Modal>
    </Container>
  )
}

Roles.propTypes = {
  rolesFetch: PropTypes.instanceOf(PromiseState).isRequired,
  deleteRole: PropTypes.func.isRequired,
  deleteRoleResponse: PropTypes.instanceOf(PromiseState),
}

export default refetch(props => ({
  rolesFetch: '/roles',
  deleteRole: ({ roleModelId }) => ({
    deleteRoleResponse: {
      url: '/role/remove',
      method: 'DELETE',
      force: true,
      body: JSON.stringify({
        RoleModelId: roleModelId,
      }),
      andThen: () => ({
        rolesFetch: {
          url: '/roles',
          force: true,
          refreshing: true,
        },
      }),
    },
  }),
}))(Roles)
