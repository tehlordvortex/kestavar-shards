import React from 'react'

import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormInput,
  FormSelect,
  FormFeedback,
  Alert,
  Button,
} from 'shards-react'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'

import { object, string, number } from 'yup'
import { Formik } from 'formik'

import { refetch } from '../utils/api'

const GenerateSchema = object().shape({
  resellerId: string().required(),
  amount: number().required().min(0)
})

const VerifySchema = object().shape({
  resellerId: string().required(),
  amount: number().required().min(0),
  token: string().required()
})

const GenerateCard = ({ resellersFetch, creditFetch, creditWallet }) => {
  if (!resellersFetch.value) return null
  const error = creditFetch && creditFetch.rejected && creditFetch.reason
  const success = creditFetch && creditFetch.fulfilled
  const isSubmitting = creditFetch && creditFetch.pending
  return (
    <Formik
      initialValues={{
        resellerId: '',
        amount: 0
      }}
      validationSchema={GenerateSchema}
      onSubmit={creditWallet}
    >
      {({
        values,
        touched,
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
      }) => (
        <Form>
          {error && (
            <Alert theme="danger">
              {error}
            </Alert>
          )}
          {success && (
            <Alert theme="success">
              Token generated. Please check your email.
            </Alert>
          )}
          <FormSelect
            name="resellerId"
            value={values.resellerId}
            invalid={errors.resellerId && touched.resellerId}
            onBlur={handleBlur}
            onChange={handleChange}
            className="mb-4"
          >
            {resellersFetch.value.map(reseller => (
              <option
                key={reseller.userModelId}
                value={reseller.userModelId}
              >
                {reseller.fullName}
              </option>
            ))}
          </FormSelect>
          <FormInput
            name="amount"
            value={values.amount}
            invalid={errors.amount && touched.amount}
            onBlur={handleBlur}
            onChange={handleChange}
            className="mb-4"
            placeholder = "Enter amount"
          />
          <div className="d-flex justify-content-end">
            <Button
              pill
              type="submit"
              disabled={isSubmitting}
              onClick={handleSubmit}
            >
              {isSubmitting ? <Spinner /> : 'Submit'}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  )
}

const RefetchedGenerateCard = refetch(() => ({
  creditWallet: ({ amount, resellerId }) => ({
    creditFetch: {
      url: '/GenerateCreditToken',
      method: 'POST',
      body: JSON.stringify({
        Amount: amount,
        ResellerId: resellerId
      }),
      force: true,
    },
  }),
}))(GenerateCard)

const VerifyCard = ({ resellersFetch, verifyFetch, verifyToken }) => {
  if (!resellersFetch.value) return null
  const error = verifyFetch && verifyFetch.rejected && verifyFetch.reason
  const success = verifyFetch && verifyFetch.fulfilled
  const isSubmitting = verifyFetch && verifyFetch.pending
  return (
    <Formik
      initialValues={{
        resellerId: '',
        amount: 0,
        token: ''
      }}
      validationSchema={VerifySchema}
      onSubmit={verifyToken}
    >
      {({
        values,
        touched,
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
      }) => (
        <Form>
          {error && (
            <Alert theme="danger">
              {error}
            </Alert>
          )}
          {success && (
            <Alert theme="success">
              Successfully credited the chosen reseller!
            </Alert>
          )}
          <FormSelect
            className="mb-4"
            name="resellerId"
            value={values.resellerId}
            invalid={errors.resellerId && touched.resellerId}
            onBlur={handleBlur}
            onChange={handleChange}
          >
            {resellersFetch.value.map(reseller => (
              <option
                key={reseller.userModelId}
                value={reseller.userModelId}
              >
                {reseller.fullName}
              </option>
            ))}
          </FormSelect>
          <FormInput
            className="mb-4"
            name="amount"
            value={values.amount}
            invalid={errors.amount && touched.amount}
            onBlur={handleBlur}
            onChange={handleChange}
            placeholder = "Enter amount"
          />
          <FormInput
            className="mb-4"
            name="token"
            value={values.token}
            invalid={errors.token && touched.token}
            onBlur={handleBlur}
            onChange={handleChange}
            placeholder = "Enter token"
          />
          <div className="d-flex justify-content-end">
            <Button
              pill
              type="submit"
              disabled={isSubmitting}
              onClick={handleSubmit}
            >
              {isSubmitting ? <Spinner /> : 'Submit'}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  )
}

const RefetchedVerifyCard = refetch(() => ({
  verifyToken: ({ amount, resellerId, token }) => ({
    verifyFetch: {
      url: '/CreditWallet',
      method: 'POST',
      body: JSON.stringify({
        Amount: amount,
        ResellerId: resellerId,
        VerificationToken: token
      }),
      force: true,
    },
  }),
}))(VerifyCard)

const Wallet = ({ resellersFetch }) => {
    return (
      <Container fluid className="main-content-container px-5">
        {/* Page Header */}
        <Row noGutters className="page-header py-4 d-flex align-items-center">
          <PageTitle
            sm="10"
            title={`Credit Wallet`}
            subtitle="Wallet"
            className="text-sm-left"
          />
          {resellersFetch.pending && (
            <Spinner />
          )}
        </Row>

        <Row className="py-3">
          <Col xs={12} md={6}>
            <Card>
              <CardHeader>
                <h5>Generate Credit Token</h5>
              </CardHeader>
              <CardBody>
                <RefetchedGenerateCard resellersFetch={resellersFetch} />
              </CardBody>
            </Card>
          </Col>
          <Col xs={12} md={6}>
            <Card>
              <CardHeader>
                <h5>Credit wallet with Token</h5>
              </CardHeader>
              <CardBody>
                <RefetchedVerifyCard resellersFetch={resellersFetch} />
              </CardBody>
            </Card>
          </Col>
        </Row>

      </Container>
    )
}

export default refetch(() => ({
  resellersFetch: '/users',
}))(Wallet)