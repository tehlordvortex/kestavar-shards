import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormInput,
  FormCheckbox,
  FormFeedback,
  Alert,
  Button,
} from 'shards-react'

import { Redirect } from 'react-router-dom'

import { refetch } from '../utils/api'

import { Formik, FieldArray } from 'formik'
import { object, string, array } from 'yup'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'
import { PromiseState } from 'react-refetch'

const RoleSchema = object().shape({
  name: string().required(),
  permissions: array()
    .required('You must assign permissions to the role')
    .min(1, 'At least one permission must be selected'),
})

const RoleEditor = ({
  roleFetch,
  permissionsFetch,
  createRole,
  createRoleResponse,
  updateRole,
  updateRoleResponse,
  id,
  history,
}) => {
  const [roleData, setRoleData] = React.useState({
    roleModelId: '',
    roleName: '',
    permissions: ''
  })
  useEffect(() => {
    if (roleFetch && roleFetch.value) {
      setRoleData(roleFetch.value)
    }
  }, [roleFetch])

  const fetches = [permissionsFetch]
  roleFetch && fetches.push(roleFetch)
  // createRoleResponse && fetches.push(createRoleResponse)

  const allFetches = PromiseState.all(fetches)

  const mode = id ? 'Edit' : 'Create'

  const isSubmitting =
    (createRoleResponse && createRoleResponse.pending) ||
    (updateRoleResponse && updateRoleResponse.pending)

  if (
    (createRoleResponse && createRoleResponse.fulfilled) ||
    (updateRoleResponse && updateRoleResponse.fulfilled)
  ) {
    return <Redirect to="/app/roles" />
  }

  return (
    <Container fluid className="main-content-container px-5">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="12"
          title={`${mode} Role`}
          subtitle="Roles"
          className="text-sm-left"
        />
      </Row>

      <Row className="pb-5 justify-content-center">
        <Col md="8" lg="5">
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between align-items-center">
                <h5 className="mb-0">{mode} Role</h5>
                {allFetches.pending && <Spinner />}
                {!allFetches.pending && (
                  <Button onClick={() => history.push('/app/users')} pill>
                    &larr; Go Back
                  </Button>
                )}
              </div>
            </CardHeader>
            <CardBody>
              {allFetches.rejected && (
                <Alert theme="danger">{allFetches.reason.message}</Alert>
              )}
              {allFetches.fulfilled && (
                <Formik
                  initialValues={{
                    roleModelId: roleData.roleModelId,
                    name: roleData.roleName,
                    permissions: roleData.permissions.split(","),
                  }}
                  enableReinitialize={true}
                  validationSchema={RoleSchema}
                  onSubmit={mode === 'Create' ? createRole : updateRole}
                >
                  {({
                    values,
                    touched,
                    errors,
                    handleBlur,
                    handleChange,
                    handleSubmit,
                  }) => (
                    <Form>
                      <FormInput
                        name="name"
                        placeholder="Role Name"
                        className="mb-4"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={errors.name && touched.name}
                      />
                      {errors.permissions && touched.permissions && (
                        <FormFeedback>{errors.permissions}</FormFeedback>
                      )}
                      <FieldArray
                        name="permissions"
                        validateOnChange={true}
                        render={arrayHelpers => (
                          <div className="mb-4">
                            {permissionsFetch.value.map(perm => (
                              <FormCheckbox
                                name="permissons"
                                value={perm.permissionName}
                                checked={values.permissions.includes(
                                  perm.permissionName
                                )}
                                toggle
                                small
                                onChange={e => {
                                  if (e.target.checked)
                                    arrayHelpers.push(perm.permissionName)
                                  else {
                                    const idx = values.permissions.indexOf(
                                      perm.permissionName
                                    )
                                    arrayHelpers.remove(idx)
                                  }
                                }}
                                onBlur={handleBlur}
                                key={perm.permissionId}
                              >
                                Manage {perm.permissionName}
                              </FormCheckbox>
                            ))}
                          </div>
                        )}
                      />
                      <div className="d-flex justify-content-end">
                        <Button
                          pill
                          type="submit"
                          disabled={isSubmitting}
                          onClick={handleSubmit}
                        >
                          {isSubmitting ? <Spinner /> : mode}
                        </Button>
                      </div>
                    </Form>
                  )}
                </Formik>
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

RoleEditor.propTypes = {
  roleFetch: PropTypes.instanceOf(PromiseState),
  permissionsFetch: PropTypes.instanceOf(PromiseState).isRequired,
  createRole: PropTypes.func.isRequired,
  createRoleResponse: PropTypes.instanceOf(PromiseState),
  updateRole: PropTypes.func.isRequired,
  updateRoleResponse: PropTypes.instanceOf(PromiseState),
}

export default refetch(props => ({
  ...(props.id && { roleFetch: `/role/${props.id}` }),
  permissionsFetch: `/permissions`,
  createRole: ({ name, permissions }) => ({
    createRoleResponse: {
      url: '/role/create',
      method: 'POST',
      force: true,
      body: JSON.stringify({
        RoleName: name,
        Permissions: permissions.join(','),
      }),
    },
  }),
  updateRole: ({ roleModelId, name, permissions }) => ({
    updateRoleResponse: {
      url: '/role/update',
      method: 'POST',
      force: true,
      body: JSON.stringify({
        RoleModelId: roleModelId,
        RoleName: name,
        Permissions: permissions.join(','),
      }),
    },
  }),
}))(RoleEditor)
