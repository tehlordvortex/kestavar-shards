import React from "react";
import { Container, Button } from "shards-react";

const Error404 = ({ history }) => (
  <Container fluid className="main-content-container px-4 pb-4">
    <div className="error">
      <div className="error__content">
        <h2>404</h2>
        <h3>Woops! That doesn't exist!</h3>
        <p>The requested page was not found.</p>
        <Button onClick={() => history.go(-1) } pill>&larr; Go Back</Button>
      </div>
    </div>
  </Container>
);

export default Error404;