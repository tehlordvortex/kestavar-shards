import React from 'react'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Alert,
  Collapse,
  Form,
  FormInput,
  FormSelect,
  Button,
} from 'shards-react'

import { Formik } from 'formik'
import { object, string, number, mixed } from 'yup'

import { refetch } from '../utils/api'
import { accountTypes, merchantFKs } from '../utils/constants'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'
import OpenCloseIcon from '../components/common/OpenCloseIcon'

const BorrowSchema = object().shape({
  customerId: string().required(),
  amount: number()
    .required()
    .positive(),
  accountType: mixed().oneOf(accountTypes.map(({ value }) => value)),
  merchantFK: mixed().oneOf(merchantFKs.map(({ value }) => value)),
})

const CheckEligibleSchema = object().shape({
  customerId: string().required(),
  accountType: mixed().oneOf(accountTypes.map(({ value }) => value)),
  merchantFK: mixed().oneOf(merchantFKs.map(({ value }) => value)),
})

const initialValues = {
  customerId: '',
  amount: '',
  accountType: accountTypes[0].value,
  merchantFK: merchantFKs[0].value,
}

const EligibleForm = ({ checkEligible, checkEligibleResponse }) => {
  const isSubmitting = checkEligibleResponse && checkEligibleResponse.pending
  const hasError = checkEligibleResponse && checkEligibleResponse.rejected
  const errorReason = checkEligibleResponse && checkEligibleResponse.reason
  return (
    <Formik
      initialValues={{ ...initialValues }}
      validationSchema={CheckEligibleSchema}
      onSubmit={checkEligible}
    >
      {({
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
      }) => (
        <Form>
          {hasError && <Alert theme="danger">{errorReason.message}</Alert>}
          <FormInput
            name="customerId"
            placeholder="Meter number"
            className="mb-3"
            value={values.customerId}
            onChange={handleChange}
            onBlur={handleBlur}
            invalid={errors.customerId && touched.customerId}
          />
          <FormSelect
            name="accountType"
            className="mb-3"
            value={values.accountType}
            onChange={handleChange}
            onBlur={handleBlur}
            invalid={errors.accountType && touched.accountType}
          >
            {accountTypes.map(({ value, label }) => (
              <option value={value} key={value}>
                {label}
              </option>
            ))}
          </FormSelect>
          <FormSelect
            name="merchantFK"
            className="mb-3"
            value={values.merchantFK}
            onChange={handleChange}
            onBlur={handleBlur}
            invalid={errors.merchantFK && touched.merchantFK}
          >
            {merchantFKs.map(({ value, label }) => (
              <option value={value} key={value}>
                {label}
              </option>
            ))}
          </FormSelect>
          <div className="d-flex justify-content-end">
            <Button
              pill
              type="submit"
              disabled={isSubmitting}
              onClick={handleSubmit}
            >
              {isSubmitting ? <Spinner /> : 'Check'}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  )
}

const BorrowForm = ({ borrow, borrowResponse }) => {
  const isSubmitting = borrowResponse && borrowResponse.pending
  const hasError = borrowResponse && borrowResponse.rejected
  const errorReason = borrowResponse && borrowResponse.reason
  const message =
    borrowResponse && borrowResponse.value && borrowResponse.CustomerMessage
  return (
    <Formik
      initialValues={{ ...initialValues }}
      validationSchema={BorrowSchema}
      onSubmit={borrow}
    >
      {({
        values,
        errors,
        touched,
        handleBlur,
        handleChange,
        handleSubmit,
      }) => (
        <Form>
          {hasError && <Alert theme="danger">{errorReason.message}</Alert>}
          {message && <Alert theme="success">{message}</Alert>}
          <FormInput
            name="customerId"
            placeholder="Meter number"
            className="mb-3"
            value={values.customerId}
            onChange={handleChange}
            onBlur={handleBlur}
            invalid={errors.customerId && touched.customerId}
          />
          <FormInput
            name="amount"
            className="mb-3"
            placeholder="Units"
            value={values.amount}
            onChange={handleChange}
            onBlur={handleBlur}
            invalid={errors.amount && touched.amount}
          />
          <FormSelect
            name="accountType"
            className="mb-3"
            value={values.accountType}
            onChange={handleChange}
            onBlur={handleBlur}
            invalid={errors.accountType && touched.accountType}
          >
            {accountTypes.map(({ value, label }) => (
              <option value={value} key={value}>
                {label}
              </option>
            ))}
          </FormSelect>
          <FormSelect
            name="merchantFK"
            className="mb-3"
            value={values.merchantFK}
            onChange={handleChange}
            onBlur={handleBlur}
            invalid={errors.merchantFK && touched.merchantFK}
          >
            {merchantFKs.map(({ value, label }) => (
              <option value={value} key={value}>
                {label}
              </option>
            ))}
          </FormSelect>
          <div className="d-flex justify-content-end">
            <Button
              pill
              type="submit"
              disabled={isSubmitting}
              onClick={handleSubmit}
            >
              {isSubmitting ? <Spinner /> : 'Check'}
            </Button>
          </div>
        </Form>
      )}
    </Formik>
  )
}

const Borrow = ({
  checkEligible,
  checkEligibleResponse,
  borrow,
  borrowResponse,
}) => {
  const [active, setActive] = React.useState('check')
  const switchToCheck = () => {
    setActive('check')
  }
  const switchToBorrow = () => {
    setActive('borrow')
  }
  return (
    <Container fluid className="main-content-container px-5">
      {/* Page Header */}
      <Row noGutters className="page-header py-4 align-items-center">
        <PageTitle
          sm="12"
          title="Borrow Power"
          subtitle="Power"
          className="text-sm-left"
        />
      </Row>
      <Row className="pb-5 justify-content-center">
        <Col md="8" lg="5">
          <Card>
            <CardHeader
              className="cursor-pointer d-flex justify-content-between align-items-center"
              onClick={switchToCheck}
            >
              <h5 className="mb-0">Check Eligibility</h5>
              <OpenCloseIcon open={active === 'check'} />
            </CardHeader>
            <Collapse open={active === 'check'}>
              <CardBody>
                <EligibleForm
                  checkEligible={checkEligible}
                  checkEligibleResponse={checkEligibleResponse}
                />
              </CardBody>
            </Collapse>
            <CardHeader
              className="cursor-pointer d-flex justify-content-between align-items-center"
              onClick={switchToBorrow}
            >
              <h5 className="mb-0">Borrow Power</h5>
              <OpenCloseIcon open={active === 'borrow'} />
            </CardHeader>
            <Collapse open={active === 'borrow'}>
              <CardBody>
                <BorrowForm borrow={borrow} borrowResponse={borrowResponse} />
              </CardBody>
            </Collapse>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default refetch(props => ({
  borrow: ({ customerId, amount, accountType, merchantFK }) => ({
    borrowResponse: {
      url: `/power/customer/validate`,
      method: 'POST',
      force: true,
      body: JSON.stringify({
        customerId,
        accountType,
        MerchantFK: merchantFK,
      }),
      then: () => ({
        url: '/power/borrow',
        method: 'POST',
        force: true,
        body: JSON.stringify({
          customerId,
          amount,
          accountType,
          MerchantFK: merchantFK,
        }),
      }),
    },
  }),
  checkEligible: ({ customerId, accountType, merchantFK }) => ({
    checkEligibleResponse: {
      url: `/power/customer/validate`,
      method: 'POST',
      force: true,
      body: JSON.stringify({
        customerId,
        accountType,
        MerchantFK: merchantFK,
      }),
      then: () => ({
        url: '/power/borrow/eligible',
        method: 'POST',
        force: true,
        body: JSON.stringify({
          customerId,
          accountType,
          MerchantFK: merchantFK,
        }),
      }),
    },
  }),
}))(Borrow)
