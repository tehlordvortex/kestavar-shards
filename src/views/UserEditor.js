import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Form,
  FormInput,
  FormSelect,
  FormFeedback,
  Alert,
  Button,
} from 'shards-react'

import { Redirect } from 'react-router-dom'

import { refetch } from '../utils/api'

import { Formik } from 'formik'
import { object, string, number, mixed } from 'yup'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'
import { PromiseState } from 'react-refetch'
import { userAccountTypes } from '../utils/constants'

const UserSchema = object().shape({
  name: string().required(),
  email: string()
    .email()
    .required(),
  role: string().required(),
  percentage: number()
    .required()
    .min(0)
    .max(100),
  password: string()
    .min(6, 'Password must be at least 6 characters long')
    .required('Password is required'),
  subscription: mixed()
    .oneOf(userAccountTypes.map(type => type.value))
    .required()
})

const UserEditor = ({
  userFetch,
  rolesFetch,
  createUser,
  createUserResponse,
  updateUser,
  updateUserResponse,
  id,
  history,
}) => {
  const [userData, setUserData] = React.useState({
    userModelId: '',
    fullName: '',
    roleId: '',
    userName: '',
    percentage: '',
    subscription: ''
  })
  useEffect(() => {
    if (userFetch && userFetch.value) {
      setUserData(userFetch.value)
    }
  }, [userFetch && userFetch.value])

  const fetches = [rolesFetch]
  userFetch && fetches.push(userFetch)
  // createUserResponse && fetches.push(createUserResponse)

  const allFetches = PromiseState.all(fetches)

  const mode = id ? 'Edit' : 'Create'

  const isSubmitting =
    (createUserResponse && createUserResponse.pending) ||
    (updateUserResponse && updateUserResponse.pending)

  if (
    (createUserResponse && createUserResponse.fulfilled) ||
    (updateUserResponse && updateUserResponse.fulfilled)
  ) {
    return <Redirect to="/app/users" />
  }

  console.log(userAccountTypes.map(type => type.value))

  return (
    <Container fluid className="main-content-container px-5">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="12"
          title={`${mode} User`}
          subtitle="Users"
          className="text-sm-left"
        />
      </Row>

      <Row className="pb-5 justify-content-center">
        <Col md="8" lg="5">
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between align-items-center">
                <h5 className="mb-0">{mode} User</h5>
                {allFetches.pending && <Spinner />}
                {!allFetches.pending && (
                  <Button onClick={() => history.push('/app/users')} pill>
                    &larr; Go Back
                  </Button>
                )}
              </div>
            </CardHeader>
            <CardBody>
              {allFetches.rejected && (
                <Alert theme="danger">{allFetches.reason.message}</Alert>
              )}
              {allFetches.fulfilled && (
                <Formik
                  initialValues={{
                    userModelId: userData.userModelId,
                    name: userData.fullName,
                    password: '',
                    email: userData.userName,
                    percentage: userData.percentage,
                    role: userData.roleId || rolesFetch.value[0].roleModelId,
                    subscription: userData.subscription || userAccountTypes[0].value,
                  }}
                  enableReinitialize={true}
                  validationSchema={UserSchema}
                  onSubmit={mode === 'Create' ? createUser : updateUser}
                >
                  {({
                    values,
                    touched,
                    errors,
                    handleBlur,
                    handleChange,
                    handleSubmit,
                  }) => (
                    <Form>
                      <FormInput
                        name="name"
                        placeholder="Full Name"
                        className="mb-3"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={errors.name && touched.name}
                      />
                      <FormInput
                        name="email"
                        type="email"
                        placeholder="Email (used to login)"
                        className="mb-3"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={errors.email && touched.email}
                      />
                      <FormInput
                        name="password"
                        type="password"
                        className={
                          errors.password && touched.password ? 'mb-0' : 'mb-3'
                        }
                        placeholder="Password"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={errors.password && touched.password}
                      />
                      {errors.password && touched.password && (
                        <FormFeedback className="mb-3">
                          {errors.password}
                        </FormFeedback>
                      )}
                      <FormInput
                        name="percentage"
                        placeholder="Percentage"
                        className="mb-3"
                        value={values.percentage}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={errors.percentage && touched.percentage}
                      />
                      
                      <FormSelect
                        name="role"
                        className="mb-4"
                        value={values.role}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={errors.role && touched.role}
                      >
                        {rolesFetch.value.map(role => (
                          <option
                            key={role.roleModelId}
                            value={role.roleModelId}
                          >
                            {role.roleName}
                          </option>
                        ))}
                      </FormSelect>
                      <FormSelect
                        name="subscription"
                        className="mb-4"
                        value={values.subscription}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={errors.subscription && touched.subscription}
                      >
                        {userAccountTypes.map(type => (
                          <option
                            key={type.value}
                            value={type.value}
                          >
                            {type.label}
                          </option>
                        ))}
                      </FormSelect>
                      <div className="d-flex justify-content-end">
                        <Button
                          pill
                          type="submit"
                          disabled={isSubmitting}
                          onClick={handleSubmit}
                        >
                          {isSubmitting ? <Spinner /> : mode}
                        </Button>
                      </div>
                    </Form>
                  )}
                </Formik>
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

UserEditor.propTypes = {
  userFetch: PropTypes.instanceOf(PromiseState),
  rolesFetch: PropTypes.instanceOf(PromiseState).isRequired,
  createUser: PropTypes.func.isRequired,
  createUserResponse: PropTypes.instanceOf(PromiseState),
  updateUser: PropTypes.func.isRequired,
  updateUserResponse: PropTypes.instanceOf(PromiseState),
}

export default refetch(props => ({
  ...(props.id && { userFetch: `/user/${props.id}` }),
  rolesFetch: `/roles`,
  createUser: ({ name, role, email, password, percentage, subscription }) => ({
    createUserResponse: {
      url: '/user/create',
      method: 'POST',
      force: true,
      body: JSON.stringify({
        UserName: email,
        RoleId: role,
        FullName: name,
        Password: password,
        Percentage: parseFloat(percentage),
        Subscription: subscription
      }),
    },
  }),
  updateUser: ({ userModelId, name, role, email, password, percentage, subscription }) => ({
    updateUserResponse: {
      url: '/user/update',
      method: 'POST',
      force: true,
      body: JSON.stringify({
        UserModelId: userModelId,
        UserName: email,
        RoleId: role,
        FullName: name,
        Password: password,
        Percentage: parseFloat(percentage),
        Subscription: subscription
      }),
    },
  }),
}))(UserEditor)
