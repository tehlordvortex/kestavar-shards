import React from 'react'
import PropTypes from 'prop-types'
import {
  Card,
  CardHeader,
  CardBody,
  Alert,
  Container,
  Row,
  Col,
  FormSelect,
  Progress,
  Button,
} from 'shards-react'
import moment from 'moment'

import PageTitle from '../components/common/PageTitle'
import Spinner from '../components/common/Spinner'
import PermissionGuard from '../components/common/PermissionGuard'
import { PromiseState } from 'react-refetch'
import { refetch } from '../utils/api'

import { Formik } from 'formik'
import { object, string, date } from 'yup'

import { useUser } from '../contexts/user'
import { formatCurrency } from '../utils/formatCurrency'
import { DatePickerField } from '../components/common/DatePickerField'

const formatDate = date => moment(date).format('lll')
const formatPercent = number => parseInt(number).toFixed(2)

const ResellerShareDisplay = ({ resellerShareFetch }) => {
  if (!resellerShareFetch) return null
  if (resellerShareFetch.pending) {
    return (
      <div className="d-flex py-3 justify-content-center align-items-center">
        <Spinner />
      </div>
    )
  }
  if (resellerShareFetch.rejected) {
    return <Alert theme="danger">{resellerShareFetch.reason.message}</Alert>
  }
  const share = resellerShareFetch.value
  const startDate = share && share.startDate
  const endDate = share && share.endDate
  return (
    <Row>
      <Col>
        <p className="text-muted">
          From {formatDate(startDate)} to {formatDate(endDate)}
        </p>
        <Row>
          <Col md="6">
            <div className="py-5 mb-3 mb-md-0 share-total d-flex flex-column justify-content-center align-items-center">
              <h2 className="mb-2">{formatCurrency(share.totalAmount)}</h2>
              <small className="d-block text-muted">
                {formatCurrency(share.netAmount)} with charges.
              </small>
            </div>
          </Col>
          <Col
            md="6"
            className="d-flex flex-column justify-content-center py-4 py-md-0"
          >
            <Row>
              <Col>
                <small className="text-muted text-right font-weight-bold">
                  {share.resellerName}
                </small>
                <h3>{formatCurrency(share.resellAmount)}</h3>
              </Col>
              <Col className="text-right">
                <small className="text-muted font-weight-bold">Kestavar</small>
                <h3>{formatCurrency(share.kestarvarAmount)}</h3>
              </Col>
            </Row>
            <Progress multi>
              <Progress bar theme="primary" value={share.resellPercet}>
                {formatPercent(share.resellPercet)}%
              </Progress>
              <Progress bar theme="success" value={share.kestarvarPercent}>
                {formatPercent(share.kestarvarPercent)}%
              </Progress>
            </Progress>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

ResellerShareDisplay.propTypes = {
  resellerShareFetch: PropTypes.instanceOf(PromiseState),
}

const ResellerSelect = refetch(props => ({
  usersFetch: '/users',
}))(({ usersFetch, reseller, selectReseller }) => (
  <Col lg="4">
    <small className="d-block text-muted">Reseller</small>
    {usersFetch.rejected && (
      <Alert theme="danger">
        Unable to fetch resellers: {usersFetch.reason.message}
      </Alert>
    )}
    {usersFetch.pending && <Spinner className="my-2" />}
    {usersFetch.fulfilled && (
      <FormSelect
        className="mb-3"
        value={reseller}
        onChange={({ target: { value } }) => selectReseller(value)}
      >
        <option value="">Select a reseller</option>
        {usersFetch.value.map(user => (
          <option key={user.userModelId} value={user.fullName}>
            {user.fullName}
          </option>
        ))}
      </FormSelect>
    )}
  </Col>
))

const ShareSchema = object().shape({
  startDate: date().required(),
  endDate: date().required,
  reseller: string(),
})

const Share = ({ getShare, resellerShareFetch }) => {
  const user = useUser()
  return (
    <Container fluid className="main-content-container px-5">
      {/* Page Header */}
      <Row noGutters className="page-header py-4 align-items-center">
        <PageTitle
          sm="11"
          title="View Reseller Share"
          subtitle="Transactions"
          className="text-sm-left"
        />
      </Row>

      <Row className="pb-5 justify-content-center">
        <Col md="10" lg="8">
          <Card>
            <CardHeader>
              <h5 className="mb-0">Reseller Share</h5>
            </CardHeader>
            <CardBody>
              <Formik
                initialValues={{
                  startDate: new Date(),
                  endDate: new Date(),
                  reseller: user.username,
                }}
                validationSchema={ShareSchema}
                onSubmit={values => getShare(values)}
              >
                {({ values, errors, touched, setFieldValue, handleSubmit }) => (
                  <Row className="mb-3 pb-3 border-bottom">
                    <Col className="mb-2 mb-lg-0" sm="6" lg="4">
                      <small className="d-block text-muted">Start Date</small>
                      <DatePickerField
                        name="startDate"
                        value={values.startDate}
                        onChange={setFieldValue}
                        invalid={touched.startDate && errors.startDate}
                      />
                    </Col>
                    <Col className="mb-2 mb-lg-0" sm="6" lg="4">
                      <small className="d-block text-muted">End Date</small>
                      <DatePickerField
                        name="endDate"
                        value={values.endDate}
                        onChange={setFieldValue}
                        invalid={touched.endDate && errors.endDate}
                      />
                    </Col>
                    <PermissionGuard permissions={['ALL']}>
                      {({ show }) =>
                        show && (
                          <ResellerSelect
                            reseller={values.reseller}
                            selectReseller={r => setFieldValue('reseller', r)}
                          />
                        )
                      }
                    </PermissionGuard>
                    <Col xs="12">
                      <div className="d-flex justify-content-end py-2">
                        <Button type="submit" pill onClick={handleSubmit}>
                          Show share
                        </Button>
                      </div>
                    </Col>
                  </Row>
                )}
              </Formik>

              <ResellerShareDisplay resellerShareFetch={resellerShareFetch} />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

export default refetch(props => ({
  getShare: ({ startDate, endDate, reseller }) => ({
    resellerShareFetch: {
      url: `/transactions/share?StartDate=${startDate.toLocaleDateString()}&EndDate=${endDate.toLocaleDateString()}&ResellerName=${reseller ||
        null}`,
      force: true,
    },
  }),
}))(Share)
