import React from 'react'
import PropTypes from 'prop-types'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Alert,
  Button,
} from 'shards-react'
import { Link } from 'react-router-dom'
import DataTable from 'react-data-table-component'

import { refetch } from '../utils/api'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'
import { PromiseState } from 'react-refetch'

const columns = [
  {
    name: 'Meter',
    selector: 'customerMeterId',
  },
  {
    name: 'Name',
    selector: 'customerName',
  },
  {
    name: 'Account Type',
    selector: 'accountType',
  },
  {
    name: 'Phone',
    selector: 'phone'
  },
  {
    name: 'Email',
    selector: 'email'
  },
  {
    cell: row => (
      <Button tag={Link} to={`/app/customers/${row.customerMeterId}`} pill>
        View
      </Button>
    ),
    ignoreRowClick: true,
    allowOverflow: true,
    button: true,
  }
]

const Customers = ({ customersFetch }) => {
  return (
    <Container fluid className="main-content-container px-5">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="12"
          title="All Customers"
          subtitle="Customers"
          className="text-sm-left"
        />
      </Row>

      <Row className="py-3">
        <Col>
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between align-items-center">
                <h5 className="mb-0">Customer List</h5>
                {customersFetch.pending && <Spinner />}
              </div>
            </CardHeader>
            <CardBody>
              {customersFetch.rejected && (
                <Alert theme="danger">{customersFetch.reason.message}</Alert>
              )}
              {customersFetch.fulfilled && (
                <DataTable
                  noHeader
                  columns={columns}
                  data={customersFetch.value}
                />
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}

Customers.propTypes = {
  customersFetch: PropTypes.instanceOf(PromiseState).isRequired,
}

export default refetch(props => ({
  customersFetch: '/customers',
}))(Customers)
