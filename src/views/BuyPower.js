import React from 'react'
import PropTypes from 'prop-types'
import {
  Container,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Form,
  FormInput,
  FormSelect,
  Button,
  Row,
  Col,
  Alert,
} from 'shards-react'

import { PromiseState } from 'react-refetch'

import { accountTypes, merchantFKs } from '../utils/constants'

import { Formik } from 'formik'

import { object, string, number, mixed } from 'yup'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'

import { refetch } from '../utils/api'

const BuyPowerSchema = object().shape({
  amount: number()
    .required()
    .positive(),
  meterNo: string().required(),
  customerName: string().required(),
  accountType: mixed().oneOf(accountTypes.map(({ value }) => value)),
  merchantFK: mixed().oneOf(merchantFKs.map(({ value }) => value)),
})

class BuyPower extends React.Component {
  static propTypes = {
    postPurchase: PropTypes.func.isRequired,
    postPurchaseResponse: PropTypes.instanceOf(PromiseState),
  }

  initialValues = {
    customerName: '',
    meterNo: '',
    amount: '',
    accountType: accountTypes[0].value,
    merchantFK: merchantFKs[0].value,
  }

  _buy = values => {
    const { postPurchase } = this.props
    postPurchase(values)
  }

  _renderForm = () => {
    const { postPurchaseResponse } = this.props
    const isSubmitting = postPurchaseResponse && postPurchaseResponse.pending
    const hasError = postPurchaseResponse && postPurchaseResponse.rejected
    const errorReason = postPurchaseResponse && postPurchaseResponse.reason
    return (
      <Formik
        initialValues={{ ...this.initialValues }}
        validationSchema={BuyPowerSchema}
        onSubmit={this._buy}
      >
        {({
          values,
          touched,
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
        }) => (
          <Form className="buy-power-form">
            <Card>
              <CardHeader>
                <h5 className="mb-0">Buy Power</h5>
              </CardHeader>
              <CardBody>
                {hasError && (
                  <Alert theme="danger">{errorReason.message}</Alert>
                )}
                <FormInput
                  name="customerName"
                  placeholder="Customer Name"
                  className="mb-3"
                  value={values.customerName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={errors.customerName && touched.customerName}
                />
                <FormInput
                  name="meterNo"
                  placeholder="Meter number"
                  className="mb-3"
                  value={values.meterNo}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={errors.meterNo && touched.meterNo}
                />
                <FormInput
                  name="amount"
                  className="mb-3"
                  placeholder="Token amount"
                  value={values.amount}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={errors.amount && touched.amount}
                />
                <FormSelect
                  name="accountType"
                  className="mb-3"
                  value={values.accountType}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={errors.accountType && touched.accountType}
                >
                  {accountTypes.map(({ value, label }) => (
                    <option value={value} key={value}>
                      {label}
                    </option>
                  ))}
                </FormSelect>
                <FormSelect
                  name="merchantFK"
                  value={values.merchantFK}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={errors.merchantFK && touched.merchantFK}
                >
                  {merchantFKs.map(({ value, label }) => (
                    <option value={value} key={value}>
                      {label}
                    </option>
                  ))}
                </FormSelect>
              </CardBody>
              <CardFooter>
                <div className="d-flex justify-content-end">
                  <Button
                    pill
                    type="submit"
                    disabled={isSubmitting}
                    onClick={handleSubmit}
                  >
                    {isSubmitting ? <Spinner /> : 'Buy'}
                  </Button>
                </div>
              </CardFooter>
            </Card>
          </Form>
        )}
      </Formik>
    )
  }
  render() {
    return (
      <Container fluid className="main-content-container px-5">
        {/* Page Header */}
        <Row noGutters className="page-header py-4 align-items-center">
          <PageTitle
            sm="12"
            title="Purchase Power"
            subtitle="Power"
            className="text-sm-left"
          />
        </Row>
        <Row className="pb-5 justify-content-center">
          <Col md="8" lg="5">
            {this._renderForm()}
          </Col>
        </Row>
      </Container>
    )
  }
}

export default refetch(props => ({
  postPurchase: values => ({
    postPurchaseResponse: {
      url: `/power/customer/validate`,
      method: 'POST',
      force: true,
      body: JSON.stringify({
        customerId: values.meterNo,
        accountType: values.accountType,
        MerchantFK: values.merchantFK,
      }),
      then: () => ({
        url: '/power/transaction/buy',
        method: 'POST',
        force: true,
        body: JSON.stringify({
          customerId: values.meterNo,
          customerName: values.customerName,
          accountType: values.accountType,
          MerchantFK: values.merchantFK,
          amount: values.amount,
        }),
      }),
    },
  }),
}))(BuyPower)
