import React, { useState } from 'react'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Alert,
  Nav,
  NavItem,
  NavLink,
} from 'shards-react'

import { refetch } from '../utils/api'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'
import { formatCurrency } from '../utils/formatCurrency';

const defaultData = {
  rate: 0,
  volume: 0,
  value: 0,
  prepaidRate: 0,
  prepaidVolume: 0,
  prepaidValue: 0,
  postRate: 0,
  postVolume: 0,
  postValue: 0,
}

const Dashboard = ({ dashboardDataFetch }) => {
  const [activeTab, setActiveTab] = useState('today')
  const dashboardData = dashboardDataFetch.value
  const data = (dashboardData && dashboardData[activeTab]) || { ...defaultData }
  const switchToToday = () => {
    setActiveTab('today')
  }
  const switchToMonth = () => {
    setActiveTab('month')
  }

  const balance = (dashboardData && dashboardData.balance) || 0

  return (
  
    <Container fluid className="main-content-container px-5">

      {/* Page Header */}
      <Row noGutters className="page-header py-4 align-items-center">
        <PageTitle
          sm="11"
          title="Overview"
          subtitle="Dashboard"
          className="text-sm-left"
        />
        <Col>{dashboardDataFetch.pending && <Spinner />}</Col>
      </Row>

      <Row className="py-3">
        <Col xs={12} sm={6} md={4}>
          <Card>
            <CardBody>
              <p className="text-muted my-0">Balance</p>
              <h1 className="mb-0">{formatCurrency(balance)}</h1>
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Row className="py-3">

        <Col>
          <Card>
            <CardHeader className="d-flex flex-column flex-md-row justify-content-between align-items-md-center">
              <h5 className="mb-3 mb-md-0">Transaction Summary</h5>
              <Nav pills>
                <NavItem>
                  <NavLink
                    href="#"
                    active={activeTab === 'today'}
                    onClick={switchToToday}
                  >
                    Today
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    href="#"
                    onClick={switchToMonth}
                    active={activeTab === 'month'}
                  >
                    Month
                  </NavLink>
                </NavItem>
              </Nav>
            </CardHeader>
            {dashboardDataFetch.rejected && (
              <CardBody>
                <Alert theme="danger">
                  {dashboardDataFetch.reason.message}
                </Alert>
              </CardBody>
            )}
            {dashboardDataFetch.fulfilled && (
              <CardBody>
                <Row>
                  <Col md="6" className="">
                    <div className="mb-4">
                      <p className="text-muted mb-0">Success Rate</p>
                      <h1>{data.rate}%</h1>
                    </div>
                    <div className="mb-4">
                      <p className="text-muted mb-0">Volume</p>
                      <h1>{data.volume}</h1>
                    </div>
                    <div className="mb-3">
                      <p className="text-muted mb-0">Value</p>
                      <h1>{formatCurrency(data.value)}</h1>
                    </div>
                  </Col>
                  <Col md="6">
                    <h6 className="text-black-50 mb-0 font-weight-bold">
                      Bill - Prepaid
                    </h6>
                    <Row className="mb-4">
                      <Col>
                        <small className="text-muted d-block">Share</small>
                        <h3>{data.prepaidRate}%</h3>
                      </Col>
                      <Col>
                        <small className="text-muted d-block">Volume</small>
                        <h3>{data.prepaidVolume}</h3>
                      </Col>
                      <Col>
                        <small className="text-muted d-block">Value</small>
                        <h3>{formatCurrency(data.prepaidValue)}</h3>
                      </Col>
                    </Row>
                    <h6 className="text-black-50 mb-0 font-weight-bold">
                      Bill - Postpaid
                    </h6>
                    <Row>
                      <Col>
                        <small className="text-muted d-block">Share</small>
                        <h3>{data.postRate}%</h3>
                      </Col>
                      <Col>
                        <small className="text-muted d-block">Volume</small>
                        <h3>{data.postVolume}</h3>
                      </Col>
                      <Col>
                        <small className="text-muted d-block">Value</small>
                        <h3>{formatCurrency(data.postValue)}</h3>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </CardBody>
            )}
          </Card>
        </Col>
      </Row>
    </Container>
  
 
  )
}

export default refetch(() => ({
  dashboardDataFetch: `/dashboarddata`,
}))(Dashboard)
