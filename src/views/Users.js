import React, { useState } from 'react'
import PropTypes from 'prop-types'
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Alert,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'shards-react'
import { Link } from 'react-router-dom'
import DataTable from 'react-data-table-component'

import { refetch } from '../utils/api'

import Spinner from '../components/common/Spinner'
import PageTitle from '../components/common/PageTitle'
import PermissionGuard from '../components/common/PermissionGuard'
import { PromiseState } from 'react-refetch'

const Users = ({ usersFetch, rolesFetch, deleteUser, deleteUserResponse }) => {
  const [selectedUser, setSelectedUser] = useState(null)
  React.useEffect(() => {
    if (deleteUserResponse && deleteUserResponse.fulfilled) {
      setSelectedUser(null)
    }
  }, [deleteUserResponse])
  const promptDeleteUser = role => {
    setSelectedUser(role)
  }
  const toggle = () => {
    setSelectedUser(null)
  }
  const columns = [
    {
      name: 'Name',
      selector: 'fullName',
    },
    {
      name: 'Email',
      selector: 'userName',
    },
    {
      name: 'Role',
      selector: 'roleId',
      format: ({ roleId }) => {
        const role = rolesFetch.value.find(role => role.roleModelId === roleId)
        return role ? role.roleName : 'Unknown Role'
      },
    },
    {
      cell: row => (
        <div className="d-flex">
          <PermissionGuard permissions={['CREATE_USER']}>
            {({ show }) =>
              show && (
                <Button
                  pill
                  className="mr-2"
                  tag={Link}
                  to={`/app/users/${row.userModelId}/edit`}
                >
                  <i className="material-icons">edit</i>
                </Button>
              )
            }
          </PermissionGuard>
          <PermissionGuard permissions={['REMOVE_USER']}>
            {({ show }) =>
              show && (
                <Button
                  onClick={() => promptDeleteUser(row)}
                  pill
                  outline
                  theme="danger"
                >
                  <i className="material-icons">delete</i>
                </Button>
              )
            }
          </PermissionGuard>
        </div>
      ),
      allowOverflow: true,
      ignoreRowClick: true,
    },
  ]
  const allFetches = PromiseState.all([usersFetch, rolesFetch])
  const isDeleting = deleteUserResponse && deleteUserResponse.pending
  const hasError = deleteUserResponse && deleteUserResponse.rejected
  const errorReason = hasError && deleteUserResponse.reason
  return (
    <Container fluid className="main-content-container px-5">
      {/* Page Header */}
      <Row noGutters className="page-header py-4">
        <PageTitle
          sm="12"
          title="All Users"
          subtitle="Users"
          className="text-sm-left"
        />
      </Row>

      <Row className="py-3">
        <Col>
          <Card>
            <CardHeader>
              <div className="d-flex justify-content-between align-items-center">
                <h5 className="mb-0">User List</h5>
                {allFetches.pending && <Spinner />}
                {!allFetches.pending && (
                  <Button pill tag={Link} to="/app/users/create">
                    Create user
                  </Button>
                )}
              </div>
            </CardHeader>
            <CardBody>
              {allFetches.rejected && (
                <Alert theme="danger">{usersFetch.reason.message}</Alert>
              )}
              {allFetches.fulfilled && (
                <DataTable noHeader data={usersFetch.value} columns={columns} />
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Modal size="sm" centered open={!!selectedUser} toggle={toggle}>
        <ModalHeader>Delete user</ModalHeader>
        <ModalBody>
          {hasError && <Alert theme="danger">{errorReason.message}</Alert>}
          {!!selectedUser &&
            `Are you sure you want to delete ${selectedUser.fullName}?`}
        </ModalBody>
        <ModalFooter>
          <Button onClick={toggle} outline pill>
            Cancel
          </Button>
          <Button
            onClick={() => deleteUser(selectedUser)}
            disabled={isDeleting}
            pill
            theme="danger"
          >
            {isDeleting ? <Spinner /> : 'Delete'}
          </Button>
        </ModalFooter>
      </Modal>
    </Container>
  )
}

Users.propTypes = {
  usersFetch: PropTypes.instanceOf(PromiseState).isRequired,
  rolesFetch: PropTypes.instanceOf(PromiseState).isRequired,
  deleteUser: PropTypes.func.isRequired,
  deleteUserResponse: PropTypes.instanceOf(PromiseState),
}

export default refetch(props => ({
  usersFetch: '/users',
  rolesFetch: '/roles',
  deleteUser: ({ userModelId, userName }) => ({
    deleteUserResponse: {
      url: '/user/remove',
      method: 'DELETE',
      force: true,
      body: JSON.stringify({
        UserModelId: userModelId,
        UserName: userName,
      }),
      andThen: () => ({
        usersFetch: {
          url: '/users',
          force: true,
          refreshing: true,
        },
      }),
    },
  }),
}))(Users)
