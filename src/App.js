import React, { Suspense } from 'react'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'

import routes from './routes'

import 'bootstrap/dist/css/bootstrap.css'
import './assets/shards-dashboards.1.1.0.css'
import './assets/styles.css'
import PageLoader from './components/common/PageLoader'
import ErrorBoundary from './components/common/ErrorBoundary';
import { UserProvider, useUser } from './contexts/user';
import { isAuthorized } from './utils/roles';
import { NotificationsProvider, NotificationTray } from './components/common/Notifications'

const AuthorizationGuard = ({ Component, permissions = [], ...props }) => {
  const user = useUser()
  if (permissions.length > 0 && !isAuthorized(user.permissionsArray, permissions)) {
    return (
      <Redirect to="/app/error/unauthorized" />
    )
  } else {
    return <Component {...props} />
  }
}

const App = () => (
  <UserProvider>
    <NotificationsProvider>
      <NotificationTray />
      <Router basename={process.env.REACT_APP_BASENAME || ''}>
        <ErrorBoundary>
          <Suspense fallback={<PageLoader />}>
              <Switch>
                {routes.map((route, index) => {
                  return (
                    <Route
                      key={index}
                      path={route.path}
                      component={({ match }) => (
                        <route.layout>
                          <Switch>
                            {route.children.map((route, index) => {
                              return (
                                <Route
                                  key={index}
                                  path={route.path && `${match.path}${route.path}`}
                                  exact={route.exact}
                                  render={props => (
                                    <AuthorizationGuard
                                      Component={route.component}
                                      permissions={route.permissions}
                                      {...props}
                                    />
                                  )}
                                />
                              )
                            })}
                          </Switch>
                        </route.layout>
                      )}
                    />
                  )
                })}
              </Switch>
          </Suspense>
        </ErrorBoundary>
      </Router>
    </NotificationsProvider>
  </UserProvider>
)

export default App
